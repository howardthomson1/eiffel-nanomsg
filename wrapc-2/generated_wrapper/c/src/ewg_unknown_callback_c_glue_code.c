#include <ewg_eiffel.h>
#include <ewg_unknown_callback_c_glue_code.h>

#ifdef _MSC_VER
#pragma warning (disable:4715) // Not all control paths return a value
#endif
void* nng_pipe_cb_object =  NULL;
nng_pipe_cb_eiffel_feature nng_pipe_cb_address_1 = NULL;
nng_pipe_cb_eiffel_feature nng_pipe_cb_address_2 = NULL;
nng_pipe_cb_eiffel_feature nng_pipe_cb_address_3 = NULL;

void set_nng_pipe_cb_object (void* a_object)
{
	if (a_object) {
		nng_pipe_cb_object = eif_protect(a_object);
	} else { 
		nng_pipe_cb_object = NULL;
	}
}

void release_nng_pipe_cb_object ()
{
	eif_wean (nng_pipe_cb_object);
}

void nng_pipe_cb_stub_1 (nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3)
{
	if (nng_pipe_cb_object != NULL && nng_pipe_cb_address_1 != NULL)
	{
		nng_pipe_cb_address_1 (eif_access(nng_pipe_cb_object), anonymous_1, anonymous_2, anonymous_3);
	}
}

void nng_pipe_cb_stub_2 (nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3)
{
	if (nng_pipe_cb_object != NULL && nng_pipe_cb_address_2 != NULL)
	{
		nng_pipe_cb_address_2 (eif_access(nng_pipe_cb_object), anonymous_1, anonymous_2, anonymous_3);
	}
}

void nng_pipe_cb_stub_3 (nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3)
{
	if (nng_pipe_cb_object != NULL && nng_pipe_cb_address_3 != NULL)
	{
		nng_pipe_cb_address_3 (eif_access(nng_pipe_cb_object), anonymous_1, anonymous_2, anonymous_3);
	}
}

void set_nng_pipe_cb_entry_1 (void* a_feature){
	nng_pipe_cb_address_1 = (nng_pipe_cb_eiffel_feature) a_feature;
}

void set_nng_pipe_cb_entry_2 (void* a_feature){
	nng_pipe_cb_address_2 = (nng_pipe_cb_eiffel_feature) a_feature;
}

void set_nng_pipe_cb_entry_3 (void* a_feature){
	nng_pipe_cb_address_3 = (nng_pipe_cb_eiffel_feature) a_feature;
}

void* get_nng_pipe_cb_stub_1 (){
	return (void*) nng_pipe_cb_stub_1;
}

void* get_nng_pipe_cb_stub_2 (){
	return (void*) nng_pipe_cb_stub_2;
}

void* get_nng_pipe_cb_stub_3 (){
	return (void*) nng_pipe_cb_stub_3;
}

void call_nng_pipe_cb (void *a_function, nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3)
{
	((void (*) (nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3))a_function) (anonymous_1, anonymous_2, anonymous_3);
}

void* void_voidp_anonymous_callback_object =  NULL;
void_voidp_anonymous_callback_eiffel_feature void_voidp_anonymous_callback_address_1 = NULL;
void_voidp_anonymous_callback_eiffel_feature void_voidp_anonymous_callback_address_2 = NULL;
void_voidp_anonymous_callback_eiffel_feature void_voidp_anonymous_callback_address_3 = NULL;

void set_void_voidp_anonymous_callback_object (void* a_object)
{
	if (a_object) {
		void_voidp_anonymous_callback_object = eif_protect(a_object);
	} else { 
		void_voidp_anonymous_callback_object = NULL;
	}
}

void release_void_voidp_anonymous_callback_object ()
{
	eif_wean (void_voidp_anonymous_callback_object);
}

void void_voidp_anonymous_callback_stub_1 (void *anonymous_1)
{
	if (void_voidp_anonymous_callback_object != NULL && void_voidp_anonymous_callback_address_1 != NULL)
	{
		void_voidp_anonymous_callback_address_1 (eif_access(void_voidp_anonymous_callback_object), anonymous_1);
	}
}

void void_voidp_anonymous_callback_stub_2 (void *anonymous_1)
{
	if (void_voidp_anonymous_callback_object != NULL && void_voidp_anonymous_callback_address_2 != NULL)
	{
		void_voidp_anonymous_callback_address_2 (eif_access(void_voidp_anonymous_callback_object), anonymous_1);
	}
}

void void_voidp_anonymous_callback_stub_3 (void *anonymous_1)
{
	if (void_voidp_anonymous_callback_object != NULL && void_voidp_anonymous_callback_address_3 != NULL)
	{
		void_voidp_anonymous_callback_address_3 (eif_access(void_voidp_anonymous_callback_object), anonymous_1);
	}
}

void set_void_voidp_anonymous_callback_entry_1 (void* a_feature){
	void_voidp_anonymous_callback_address_1 = (void_voidp_anonymous_callback_eiffel_feature) a_feature;
}

void set_void_voidp_anonymous_callback_entry_2 (void* a_feature){
	void_voidp_anonymous_callback_address_2 = (void_voidp_anonymous_callback_eiffel_feature) a_feature;
}

void set_void_voidp_anonymous_callback_entry_3 (void* a_feature){
	void_voidp_anonymous_callback_address_3 = (void_voidp_anonymous_callback_eiffel_feature) a_feature;
}

void* get_void_voidp_anonymous_callback_stub_1 (){
	return (void*) void_voidp_anonymous_callback_stub_1;
}

void* get_void_voidp_anonymous_callback_stub_2 (){
	return (void*) void_voidp_anonymous_callback_stub_2;
}

void* get_void_voidp_anonymous_callback_stub_3 (){
	return (void*) void_voidp_anonymous_callback_stub_3;
}

void call_void_voidp_anonymous_callback (void *a_function, void *anonymous_1)
{
	((void (*) (void *anonymous_1))a_function) (anonymous_1);
}

void* nng_aio_cancelfn_object =  NULL;
nng_aio_cancelfn_eiffel_feature nng_aio_cancelfn_address_1 = NULL;
nng_aio_cancelfn_eiffel_feature nng_aio_cancelfn_address_2 = NULL;
nng_aio_cancelfn_eiffel_feature nng_aio_cancelfn_address_3 = NULL;

void set_nng_aio_cancelfn_object (void* a_object)
{
	if (a_object) {
		nng_aio_cancelfn_object = eif_protect(a_object);
	} else { 
		nng_aio_cancelfn_object = NULL;
	}
}

void release_nng_aio_cancelfn_object ()
{
	eif_wean (nng_aio_cancelfn_object);
}

void nng_aio_cancelfn_stub_1 (nng_aio *anonymous_1, void *anonymous_2, int anonymous_3)
{
	if (nng_aio_cancelfn_object != NULL && nng_aio_cancelfn_address_1 != NULL)
	{
		nng_aio_cancelfn_address_1 (eif_access(nng_aio_cancelfn_object), anonymous_1, anonymous_2, anonymous_3);
	}
}

void nng_aio_cancelfn_stub_2 (nng_aio *anonymous_1, void *anonymous_2, int anonymous_3)
{
	if (nng_aio_cancelfn_object != NULL && nng_aio_cancelfn_address_2 != NULL)
	{
		nng_aio_cancelfn_address_2 (eif_access(nng_aio_cancelfn_object), anonymous_1, anonymous_2, anonymous_3);
	}
}

void nng_aio_cancelfn_stub_3 (nng_aio *anonymous_1, void *anonymous_2, int anonymous_3)
{
	if (nng_aio_cancelfn_object != NULL && nng_aio_cancelfn_address_3 != NULL)
	{
		nng_aio_cancelfn_address_3 (eif_access(nng_aio_cancelfn_object), anonymous_1, anonymous_2, anonymous_3);
	}
}

void set_nng_aio_cancelfn_entry_1 (void* a_feature){
	nng_aio_cancelfn_address_1 = (nng_aio_cancelfn_eiffel_feature) a_feature;
}

void set_nng_aio_cancelfn_entry_2 (void* a_feature){
	nng_aio_cancelfn_address_2 = (nng_aio_cancelfn_eiffel_feature) a_feature;
}

void set_nng_aio_cancelfn_entry_3 (void* a_feature){
	nng_aio_cancelfn_address_3 = (nng_aio_cancelfn_eiffel_feature) a_feature;
}

void* get_nng_aio_cancelfn_stub_1 (){
	return (void*) nng_aio_cancelfn_stub_1;
}

void* get_nng_aio_cancelfn_stub_2 (){
	return (void*) nng_aio_cancelfn_stub_2;
}

void* get_nng_aio_cancelfn_stub_3 (){
	return (void*) nng_aio_cancelfn_stub_3;
}

void call_nng_aio_cancelfn (void *a_function, nng_aio *anonymous_1, void *anonymous_2, int anonymous_3)
{
	((void (*) (nng_aio *anonymous_1, void *anonymous_2, int anonymous_3))a_function) (anonymous_1, anonymous_2, anonymous_3);
}


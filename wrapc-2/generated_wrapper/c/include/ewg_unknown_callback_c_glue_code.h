#ifndef EWG_CALLBACK_UNKNOWN___
#define EWG_CALLBACK_UNKNOWN___

#include <nng.h>

typedef void (*nng_pipe_cb_eiffel_feature) (void *a_class, nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3);

void* nng_pipe_cb_object;
nng_pipe_cb_eiffel_feature nng_pipe_cb_address_1;
nng_pipe_cb_eiffel_feature nng_pipe_cb_address_2;
nng_pipe_cb_eiffel_feature nng_pipe_cb_address_3;

void set_nng_pipe_cb_object (void* a_class);

void release_nng_pipe_cb_object (void);

void* get_nng_pipe_cb_stub_1 ();
void* get_nng_pipe_cb_stub_2 ();
void* get_nng_pipe_cb_stub_3 ();

void set_nng_pipe_cb_entry_1 (void* a_feature);
void set_nng_pipe_cb_entry_2 (void* a_feature);
void set_nng_pipe_cb_entry_3 (void* a_feature);

void call_nng_pipe_cb (void *a_function, nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3);


#include <nng.h>

typedef void (*void_voidp_anonymous_callback_eiffel_feature) (void *a_class, void *anonymous_1);

void* void_voidp_anonymous_callback_object;
void_voidp_anonymous_callback_eiffel_feature void_voidp_anonymous_callback_address_1;
void_voidp_anonymous_callback_eiffel_feature void_voidp_anonymous_callback_address_2;
void_voidp_anonymous_callback_eiffel_feature void_voidp_anonymous_callback_address_3;

void set_void_voidp_anonymous_callback_object (void* a_class);

void release_void_voidp_anonymous_callback_object (void);

void* get_void_voidp_anonymous_callback_stub_1 ();
void* get_void_voidp_anonymous_callback_stub_2 ();
void* get_void_voidp_anonymous_callback_stub_3 ();

void set_void_voidp_anonymous_callback_entry_1 (void* a_feature);
void set_void_voidp_anonymous_callback_entry_2 (void* a_feature);
void set_void_voidp_anonymous_callback_entry_3 (void* a_feature);

void call_void_voidp_anonymous_callback (void *a_function, void *anonymous_1);


#include <nng.h>

typedef void (*nng_aio_cancelfn_eiffel_feature) (void *a_class, nng_aio *anonymous_1, void *anonymous_2, int anonymous_3);

void* nng_aio_cancelfn_object;
nng_aio_cancelfn_eiffel_feature nng_aio_cancelfn_address_1;
nng_aio_cancelfn_eiffel_feature nng_aio_cancelfn_address_2;
nng_aio_cancelfn_eiffel_feature nng_aio_cancelfn_address_3;

void set_nng_aio_cancelfn_object (void* a_class);

void release_nng_aio_cancelfn_object (void);

void* get_nng_aio_cancelfn_stub_1 ();
void* get_nng_aio_cancelfn_stub_2 ();
void* get_nng_aio_cancelfn_stub_3 ();

void set_nng_aio_cancelfn_entry_1 (void* a_feature);
void set_nng_aio_cancelfn_entry_2 (void* a_feature);
void set_nng_aio_cancelfn_entry_3 (void* a_feature);

void call_nng_aio_cancelfn (void *a_function, nng_aio *anonymous_1, void *anonymous_2, int anonymous_3);


#endif

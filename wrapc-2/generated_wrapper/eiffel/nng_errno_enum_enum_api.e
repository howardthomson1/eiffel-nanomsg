-- enum wrapper
class NNG_ERRNO_ENUM_ENUM_API

feature {ANY}

	is_valid_enum (a_value: INTEGER): BOOLEAN 
			-- Is `a_value' a valid integer code for this enum ?
		do
			Result := a_value = nng_eintr or a_value = nng_enomem or a_value = nng_einval or a_value = nng_ebusy or a_value = nng_etimedout or a_value = nng_econnrefused or a_value = nng_eclosed or a_value = nng_eagain or a_value = nng_enotsup or a_value = nng_eaddrinuse or a_value = nng_estate or a_value = nng_enoent or a_value = nng_eproto or a_value = nng_eunreachable or a_value = nng_eaddrinval or a_value = nng_eperm or a_value = nng_emsgsize or a_value = nng_econnaborted or a_value = nng_econnreset or a_value = nng_ecanceled or a_value = nng_enofiles or a_value = nng_enospc or a_value = nng_eexist or a_value = nng_ereadonly or a_value = nng_ewriteonly or a_value = nng_ecrypto or a_value = nng_epeerauth or a_value = nng_enoarg or a_value = nng_eambiguous or a_value = nng_ebadtype or a_value = nng_econnshut or a_value = nng_einternal or a_value = nng_esyserr or a_value = nng_etranerr
		end

	nng_eintr: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EINTR"
		end

	nng_enomem: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ENOMEM"
		end

	nng_einval: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EINVAL"
		end

	nng_ebusy: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EBUSY"
		end

	nng_etimedout: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ETIMEDOUT"
		end

	nng_econnrefused: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ECONNREFUSED"
		end

	nng_eclosed: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ECLOSED"
		end

	nng_eagain: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EAGAIN"
		end

	nng_enotsup: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ENOTSUP"
		end

	nng_eaddrinuse: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EADDRINUSE"
		end

	nng_estate: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ESTATE"
		end

	nng_enoent: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ENOENT"
		end

	nng_eproto: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EPROTO"
		end

	nng_eunreachable: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EUNREACHABLE"
		end

	nng_eaddrinval: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EADDRINVAL"
		end

	nng_eperm: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EPERM"
		end

	nng_emsgsize: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EMSGSIZE"
		end

	nng_econnaborted: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ECONNABORTED"
		end

	nng_econnreset: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ECONNRESET"
		end

	nng_ecanceled: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ECANCELED"
		end

	nng_enofiles: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ENOFILES"
		end

	nng_enospc: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ENOSPC"
		end

	nng_eexist: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EEXIST"
		end

	nng_ereadonly: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EREADONLY"
		end

	nng_ewriteonly: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EWRITEONLY"
		end

	nng_ecrypto: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ECRYPTO"
		end

	nng_epeerauth: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EPEERAUTH"
		end

	nng_enoarg: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ENOARG"
		end

	nng_eambiguous: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EAMBIGUOUS"
		end

	nng_ebadtype: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EBADTYPE"
		end

	nng_econnshut: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ECONNSHUT"
		end

	nng_einternal: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_EINTERNAL"
		end

	nng_esyserr: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ESYSERR"
		end

	nng_etranerr: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_ETRANERR"
		end

end

-- enum wrapper
class NNG_STAT_TYPE_ENUM_ENUM_API

feature {ANY}

	is_valid_enum (a_value: INTEGER): BOOLEAN 
			-- Is `a_value' a valid integer code for this enum ?
		do
			Result := a_value = nng_stat_scope or a_value = nng_stat_level or a_value = nng_stat_counter or a_value = nng_stat_string or a_value = nng_stat_boolean or a_value = nng_stat_id
		end

	nng_stat_scope: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_STAT_SCOPE"
		end

	nng_stat_level: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_STAT_LEVEL"
		end

	nng_stat_counter: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_STAT_COUNTER"
		end

	nng_stat_string: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_STAT_STRING"
		end

	nng_stat_boolean: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_STAT_BOOLEAN"
		end

	nng_stat_id: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_STAT_ID"
		end

end

-- enum wrapper
class NNG_UNIT_ENUM_ENUM_API

feature {ANY}

	is_valid_enum (a_value: INTEGER): BOOLEAN 
			-- Is `a_value' a valid integer code for this enum ?
		do
			Result := a_value = nng_unit_none or a_value = nng_unit_bytes or a_value = nng_unit_messages or a_value = nng_unit_millis or a_value = nng_unit_events
		end

	nng_unit_none: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_UNIT_NONE"
		end

	nng_unit_bytes: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_UNIT_BYTES"
		end

	nng_unit_messages: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_UNIT_MESSAGES"
		end

	nng_unit_millis: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_UNIT_MILLIS"
		end

	nng_unit_events: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_UNIT_EVENTS"
		end

end

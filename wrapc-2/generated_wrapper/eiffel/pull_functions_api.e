note

	description: "This file has been generated by WrapC. Do not edit. Changes will be lost!"

	generator: "Eiffel Wrapper Generator"
-- functions wrapper
class PULL_FUNCTIONS_API


feature -- Access

	nng_pull0_open (anonymous_1: NNG_SOCKET_S_STRUCT_API): INTEGER 
		do
			Result := c_nng_pull0_open (anonymous_1.item)
		ensure
			instance_free: class
		end

	nng_pull0_open_raw (anonymous_1: NNG_SOCKET_S_STRUCT_API): INTEGER 
		do
			Result := c_nng_pull0_open_raw (anonymous_1.item)
		ensure
			instance_free: class
		end

feature -- Externals

	c_nng_pull0_open (anonymous_1: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_pull0_open ((nng_socket*)$anonymous_1);
			]"
		end

	c_nng_pull0_open_raw (anonymous_1: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_pull0_open_raw ((nng_socket*)$anonymous_1);
			]"
		end

feature -- Externals Address

end

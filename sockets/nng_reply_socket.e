note
	description: "Summary description for {NNG_REPLY_SOCKET}."
	author: "Howard Thomson"
	date: "5-May-2020"

	TODO: "[
		More descendants to implement:
			Request/Reply -- DONE
			Pair [pair0 / pair1]
			Push/Pull
			Pub/Sub
			Survey/Respond
	]"

class
	NNG_REPLY_SOCKET

inherit
	NNG_SOCKET

create
	default_create

feature

	open
			-- Open as a REP [Protocol 0] socket
		do
			last_error := nng_rep0_open (Current)
--			set_receiving
		end

	open_raw
			-- Open as a REP socket
		do
			last_error := nng_rep0_open_raw (Current)
			sending_internal := False
		end



--	listen (a_url: STRING)
--		require
--			is_open
--		do
--			last_error := nng_listen (Current, a_url, Void, 0)
--			sending_internal := False
--		end

feature {NONE}

	sending_internal: BOOLEAN

feature

	sending: BOOLEAN
		do
			Result := sending_internal
		end

	receiving: BOOLEAN
		do
			Result := not sending_internal
		end

feature

	async_post_process
		do
			if sending_internal then
				sending_internal := False
			else
				sending_internal := True
			end
		end

note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

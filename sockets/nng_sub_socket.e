note
	description: "Summary description for {NNG_SUB_SOCKET}."
	author: "Howard Thomson"
	date: "$6-May-2022$"

class
	NNG_SUB_SOCKET

inherit
	NNG_SOCKET

create
	default_create

feature

	open
			-- Open as a SUB [subscribe] socket
		do
			last_error := nng_rep0_open (Current)
		end

	open_raw
			-- Open as a raw SUB socket
		do
			last_error := nng_sub0_open_raw (Current)
		end

feature

	sending: BOOLEAN
		do
			Result := False
		end

	receiving: BOOLEAN
		do
			Result := True
		end

feature

	async_post_process
		do

		end
end

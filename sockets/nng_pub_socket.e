note
	description: "Summary description for {NNG_PUB_SOCKET}."
	author: "Howard Thomson"
	date: "$6-May-2022$"

class
	NNG_PUB_SOCKET

inherit
	NNG_SOCKET

create
	default_create

feature

	open
			-- Open as a PUB socket
		do
			last_error := nng_pub0_open (Current)
--			set_receiving
		end

	open_raw
			-- Open as a PAIR socket
		do
			last_error := nng_pub0_open_raw (Current)
--			set_receiving
		end

feature

	sending: BOOLEAN
		do
			Result := True
		end

	receiving: BOOLEAN
		do
			Result := False
		end
		
feature

	async_post_process
		do

		end
end

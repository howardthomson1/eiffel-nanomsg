note
	description: "Summary description for {NNG_PUSH_SOCKET}."
	author: "Howard Thomson"
	date: "$6-May-2022$"

class
	NNG_PUSH_SOCKET

inherit
	NNG_SOCKET

create
	default_create

feature

	open
			-- Open as a PUSH socket
		do
			last_error := nng_push0_open (Current)
			check last_error = 0 and then socket_id /= 0 end
--			set_sending
		end

	open_raw
			-- Open as a PUSH socket
		do
			last_error := nng_push0_open_raw (Current)
--			set_receiving
		end

feature

	sending: BOOLEAN
		do
			Result := True
		end

	receiving: BOOLEAN
		do
			Result := False
		end

feature

	async_post_process
		do

		end

end

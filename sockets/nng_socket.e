note
	description: "Summary description for {NNG_SOCKET}."
	author: "Howard Thomson"
	notes: "[
		The memory allocated to the SOCKET_S_STRUCT is a struct socket

		The asynchronous nng routines are used for synchronous I/O due
		to problematic interaction with the Eiffel GC.

		For asynchronous send/receive, need to track when post actions
		of the AIO allback have been done ...
		Move the 'send' routine to NNG_MESSAGE
		Make last_message detachable ?
	]"

	TODO: "[
		Make collection of a received message thread-safe:
			Need to be able to 'collect' a received message from
			a separate thread, using a MUTEX [load-clear-store]
	
		Adapt NNG code to work with LIBSODIUM for authentication, encryption etc ...

		Adapt set_sending and set_receiving for socket types
		
		Decide [correctly!] where and when sending/receiving status changes ...
		
		For timeout on receive, sending not permitted (ESTATE) ?
		
		Re-arrange code to limit feature export
	]"

deferred class
	NNG_SOCKET

inherit
	NNG_INTERFACE
		redefine default_create end
	NNG_FLAG_ENUM_ENUM_API
		redefine default_create end
	NNG_STRUCT_UINT32
		rename
			id as socket_id,
			set_id as set_socket_id
		redefine
			default_create
		end

feature {NONE}

	set_aio_procedure_target (an_agent: PROCEDURE)
		do
			aio.set_callback (an_agent)
		end

	default_create
		do
			make_struct_uint32
			create last_message
			create mutex.make
			create condition.make
			create aio.make
			aio.set_mutex_ptr (mutex.mutex_pointer)
			aio.set_condition_ptr (condition.cond_pointer)
		end

	mutex: NNG_MUTEX

	condition: NNG_CONDITION_VARIABLE

	aio: NNG_AIO

feature

	is_open: BOOLEAN
		do
			Result := socket_id /= 0
		end

	open
			-- Call the specific open routine for the type of the socket ...
		require
			not is_open
		deferred
		end

	open_raw
			-- Call the specific open_raw routine for the type of the socket ...
		require
			not is_open
		deferred
		end

	close
		require
			is_open: is_open
		do
			last_error := c_nng_close (item)
			set_socket_id (0)
		ensure
			is_closed: not is_open
		end

feature -- Synchronous send/receive

	void_reply
			-- Send an 'empty' message reply
		require
			socket_is_open: is_open
			sending_permitted: sending
		local
			l_msg: NNG_MESSAGE
		do
			create l_msg.make_size (4)
			l_msg.append_integer_32 ( -1 )
			send (l_msg)
		end

	is_busy: BOOLEAN
		do
			Result := aio.is_busy
		end

	send (a_msg: NNG_MESSAGE)
			-- Synchronous send
		require
			socket_is_open: is_open
			has_content: a_msg.has_content
			sending_permitted: sending
			not_busy: not is_busy
		do
			mutex.lock
			aio.set_message (a_msg)
			aio.send (Current)
			condition.wait (mutex)
			aio.await_completion

			if not signalled then
				last_error := aio.aio_result

				if last_error = 0 then
						-- Message sent OK
					a_msg.set_msg_ptr (default_pointer)
				elseif last_error = {NNG_C_API}.nng_etimedout then
					print ("Message timed out ...%N")
					a_msg.msg_free
				else
					print ("SOCKET.send last_error = " + last_error.out + "%N")
					check socket_send_error: False end
				end
			end
			mutex.unlock
			async_post_process
		ensure
			not_busy: not is_busy
		end

	receive
			-- Synchronous wait to receive ...
		require
			socket_is_open: is_open
			receiving_permitted: receiving
--			not_busy: not is_busy
		local
			l_msg: NNG_MESSAGE
		do
			check last_message.msg_ptr = default_pointer end
			mutex.lock
			if aio.flag then
				condition.wait (mutex)
				check not aio.flag end
			end
			aio.receive (Current)
	--		check busy_after_aio_receive: is_busy end
			condition.wait (mutex)

--			aio.await_completion

	--		check not_busy_after_cv_wait: not is_busy end
			if not signalled then
				last_error := aio.aio_result
		--		check last_error = 0 end
				if last_error = 0 then
					l_msg := aio.get_message
					last_message := l_msg
				end
			end
			mutex.unlock
			async_post_process
		ensure
			not_busy: not is_busy
		end

	last_error: INTEGER

	last_message: NNG_MESSAGE

	signalled: BOOLEAN
		-- Has this socket been signalled, re termination ?

--	signal
--		do
--			signalled := True
--			aio.signal
--		end

feature -- Asynchronous send/receive

	send_async_with_callback (a_msg: NNG_MESSAGE; a_callback: PROCEDURE)
		require
			sending_permitted: sending
		do
			send_completion_callback := a_callback
		--	set_aio_procedure_target (a_callback)
			send_async (a_msg)
		end

	send_completion_callback: detachable PROCEDURE

	send_async (a_msg: NNG_MESSAGE)
		require
			socket_is_open: is_open
			has_content: a_msg.has_content
			sending_permitted: sending
		do
--			set_receiving
			sending_message := a_msg
			mutex.lock
--			aio.set_flag (False)
			aio.set_message (a_msg)
			aio.send (Current)
			mutex.unlock
		end

	receive_async (a_callback: PROCEDURE)
			-- Asynchronous wait to receive ...
		require
			socket_is_open: is_open
			receiving_permitted: receiving
		do
			mutex.lock
--			aio.set_flag (False)
			aio.receive (Current)
			mutex.unlock
		end

	async_post_process
		deferred
		end

feature -- Async sending/receiving state

	sending: BOOLEAN
			-- True if premitted to send
		deferred
		end

	receiving: BOOLEAN
			-- True if can call receive
		deferred
		end

feature {NONE}

	sending_message: detachable NNG_MESSAGE

--	set_sending
--		do
--			sending := True
--			post_actions_done := False
--		end

--	set_receiving
--		do
--			sending := False
--			post_actions_done := False
--		end

feature -- Async completion

	await_aio_completion
			-- Wait for aio completion
		do
			mutex.lock
			if aio.flag then
--				aio.set_flag (True)
				condition.wait (mutex)
--				aio.await_completion
			end
			if not signalled then
				last_error := aio.aio_result
				if last_error = 0 then
					if sending then
						if attached sending_message as sm then
							sm.set_msg_ptr (default_pointer)
						end
					else
						last_message := aio.get_message
						check last_message.has_content end

--						print ("await_aio_completion, msg_len = "
--							+ last_message.nng_msg_len (last_message).out + "%N")

					end
				end
			end
			mutex.unlock
		end

	is_aio_completed: BOOLEAN
		-- Test for AIO completion
		do
			mutex.lock
			if not aio.flag then
				Result := False
			elseif not post_actions_done then
				if not signalled then
					last_error := aio.aio_result
					if last_error = 0 then
						if sending then
							if attached sending_message as sm then
								sm.set_msg_ptr (default_pointer)
							end
						else
							last_message := aio.get_message
						end
					end
				end
				post_actions_done := True
				Result := True
			else
				Result := True
			end
			mutex.unlock
		ensure
			Result implies post_actions_done = True
			(receiving and then post_actions_done and then last_error = 0)
				implies last_message /= Void
		end

	received_message: detachable NNG_MESSAGE
			-- Thread safe: collect a received message, if present
		do
			mutex.lock
			if attached last_message as lm then
				Result := lm
				create last_message
			else


			end

--			if not aio.flag then
--				Result := False
--			elseif not post_actions_done then
--				if not signalled then
--					last_error := aio.aio_result
--					if last_error = 0 then
--						if sending then
--							if attached sending_message as sm then
--								sm.set_msg_ptr (default_pointer)
--							end
--						else
--							last_message := aio.get_message
--						end
--					end
--				end
--				post_actions_done := True
--				Result := True
--			else
--				Result := True
--			end
			mutex.unlock
		end


feature {NONE} -- Internal

	post_actions_done: BOOLEAN
		-- Comment needed: TODO

feature -- Send/Receive serialized Eiffel objects and their attachments

--	sed_facilities: SED_STORABLE_FACILITIES
--		once
--			create Result
--		end

--	send_any (a_root_object: ANY)
--		local
--			l_msg: NNG_MESSAGE
--			l_writer: NNG_MEMORY_READER_WRITER
--			l_base: POINTER
--			l_count: INTEGER
--			l_result: INTEGER
--		do
--			create l_msg
--			create l_writer.make
--			l_writer.set_for_writing
--				-- Serialize 'a_root_object' into the writer
--			sed_facilities.store (a_root_object, l_writer)
--				-- Copy from the writer into the message
--			l_base := l_writer.buffer.item
--			l_count := l_writer.count
--			l_result := nng_msg_append (l_msg, l_base, l_count)
--			check l_result = 0 end
--				-- Send the message
--			send (l_msg)
--		end

--	send_any_async (a_root_object: ANY)
--		do

--		end

--	receive_any
--			-- Synchronous receive of a serialized Eiffel object
--			-- TODO Make NNG messages distinguishable on receipt
--			-- between serialized objects and other options
--		local
--			l_reader: NNG_MEMORY_READER_WRITER
--		do
--			receive
--			if last_error = 0 then
--				create l_reader.make
--				l_reader.set_for_reading
--					-- Deserialize the message content
--				last_received_any := sed_facilities.retrieved (l_reader, True)




--			end
--		end

--	last_received_any: detachable ANY

feature -- Configuration settings

	NNG_OPT_SOCKNAME      : C_STRING once create Result.make ("socket-name") end
	NNG_OPT_RAW           : C_STRING once create Result.make ("raw") end
	NNG_OPT_PROTO         : C_STRING once create Result.make ("protocol") end
	NNG_OPT_PROTONAME     : C_STRING once create Result.make ("protocol-name") end
	NNG_OPT_PEER          : C_STRING once create Result.make ("peer") end
	NNG_OPT_PEERNAME      : C_STRING once create Result.make ("peer-name") end
	NNG_OPT_RECVBUF       : C_STRING once create Result.make ("recv-buffer") end
	NNG_OPT_SENDBUF       : C_STRING once create Result.make ("send-buffer") end
	NNG_OPT_RECVFD        : C_STRING once create Result.make ("recv-fd") end
	NNG_OPT_SENDFD        : C_STRING once create Result.make ("send-fd") end
	NNG_OPT_RECVTIMEO     : C_STRING once create Result.make ("recv-timeout") end
	NNG_OPT_SENDTIMEO     : C_STRING once create Result.make ("send-timeout") end
	NNG_OPT_LOCADDR       : C_STRING once create Result.make ("local-address") end
	NNG_OPT_REMADDR       : C_STRING once create Result.make ("remote-address") end
	NNG_OPT_URL           : C_STRING once create Result.make ("url") end
	NNG_OPT_MAXTTL        : C_STRING once create Result.make ("ttl-max") end
	NNG_OPT_RECVMAXSZ     : C_STRING once create Result.make ("recv-size-max") end
	NNG_OPT_RECONNMINT    : C_STRING once create Result.make ("reconnect-time-min") end
	NNG_OPT_RECONNMAXT    : C_STRING once create Result.make ("reconnect-time-max") end

feature -- Raw setting

	set_raw (a_raw_setting: BOOLEAN)
		local
			i: INTEGER
		do
			if a_raw_setting then
				i := 1
			end
			last_error := c_nng_socket_set_bool (item, NNG_OPT_RAW.item, i)
		end


	is_raw: BOOLEAN
		local
			pi: TYPED_POINTER [INTEGER_32]
			i: INTEGER
		do
			pi := $i
			last_error := c_nng_socket_get_bool (item, NNG_OPT_RAW.item, pi)
			if i /= 0 then
				Result := True
			end
		end

feature -- Timeouts

	set_receive_timeout (a_duration: INTEGER)
		do
			last_error := c_nng_socket_set_ms (item, Nng_opt_recvtimeo.item, a_duration)
			check last_error = 0 end
		end

	receive_timeout: INTEGER
		do
			last_error := c_nng_socket_get_ms (item, Nng_opt_recvtimeo.item, $Result)
			check last_error = 0 end
		end

	set_send_timeout (a_duration: INTEGER)
		local
			res: INTEGER
		do
			res := c_nng_socket_set_ms (item, Nng_opt_sendtimeo.item, a_duration)
			check res = 0 end
		end

	send_timeout: INTEGER
		do
			last_error := c_nng_socket_get_ms (item, Nng_opt_sendtimeo.item, $Result)
			check last_error = 0 end
		end

feature -- dial / listen

	dial (a_url: STRING)
		require
			is_open: is_open
		local
			i: INTEGER
		do
			from
				last_error := nng_dial (Current, a_url, Void, 0)
			until
				last_error = 0 or else i > 10
			loop
				{EXECUTION_ENVIRONMENT}.sleep (10_000)
				last_error := nng_dial (Current, a_url, Void, 0)
				i := i + 1
			end
		end

	listen (a_url: STRING)
		require
			is_open
		do
			last_error := nng_listen (Current, a_url, Void, 0)
		end

end

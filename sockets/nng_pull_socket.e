note
	description: "Summary description for {NNG_PULL_SOCKET}."
	author: "Howard Thomson"
	date: "$6-May-2022$"

class
	NNG_PULL_SOCKET

inherit
	NNG_SOCKET

create
	default_create

feature

	open
			-- Open as a PULL [Protocol 0] socket
		do
			last_error := nng_pull0_open (Current)
--			set_receiving
		end

	open_raw
			-- Open as a PAIR socket
		do
			last_error := nng_pull0_open_raw (Current)
--			set_receiving
		end
feature

	sending: BOOLEAN
		do
			Result := False
		end

	receiving: BOOLEAN
		do
			Result := True
		end

feature

	async_post_process
		do

		end

end

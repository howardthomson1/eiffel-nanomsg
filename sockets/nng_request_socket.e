note
	description: "Summary description for {NNG_REQUEST_SOCKET}."
	author: "Howard Thomson"
	date: "May-2020"

class
	NNG_REQUEST_SOCKET

inherit
	NNG_SOCKET

create
	default_create

feature

	open
			-- Open as a REQ [Protocol 0] socket
		do
			last_error := nng_req0_open (Current)
			check last_error = 0 and then socket_id /= 0 end
			sending_internal := True
		end

	open_raw
			-- Open as a REQ socket
		do
			last_error := nng_req0_open_raw (Current)
			sending_internal := True
		end

	async_post_process
		do
			if sending_internal then
				sending_internal := False
			else
				sending_internal := True
			end
		end

feature {NONE}

	sending_internal: BOOLEAN

feature

	sending: BOOLEAN
		do
			Result := sending_internal
		end

	receiving: BOOLEAN
		do
			Result := not sending_internal
		end

feature

--	async_post_process
--		do

--		end

end

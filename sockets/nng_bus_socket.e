note
	description: "Summary description for {NNG_BUS_SOCKET}."
	author: "Howard Thomson"
	date: "$Date$"

class
	NNG_BUS_SOCKET

inherit
	NNG_SOCKET

create
	default_create

feature

	open
			-- Open as a REQ [Protocol 0] socket
		do
			last_error := nng_bus0_open (Current)
			check last_error = 0 and then socket_id /= 0 end
		end

	open_raw
			-- Open as a REQ socket
		do
			last_error := nng_bus0_open_raw (Current)
			check last_error = 0 and then socket_id /= 0 end
		end

	dial (a_url: STRING)
		require
			is_open: is_open
		local
			i: INTEGER
		do
			from
				last_error := nng_dial (Current, a_url, Void, 0)
			until
				last_error = 0 or else i > 10
			loop
				{EXECUTION_ENVIRONMENT}.sleep (10_000)
				last_error := nng_dial (Current, a_url, Void, 0)
				i := i + 1
			end
		end

feature

	sending: BOOLEAN
		do
			Result := True
		end

	receiving: BOOLEAN
		do
			Result := True
		end

end

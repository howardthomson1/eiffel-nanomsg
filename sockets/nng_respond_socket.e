note
	description: "Summary description for {NNG_RESPOND_SOCKET}."
	author: "Howard Thomson"
	date: "$6-May-2022$"

class
	NNG_RESPOND_SOCKET

inherit
	NNG_SOCKET

create
	default_create

feature

	open
			-- Open as a RESPOND [Protocol 0] socket
		do
			last_error := nng_respondent0_open (Current)
--			set_receiving
		end

	open_raw
			-- Open as a raw RESPOND socket
		do
			last_error := nng_respondent0_open_raw (Current)
--			set_receiving
		end

feature

	sending: BOOLEAN
		do
			Result := True
		end

	receiving: BOOLEAN
		do
			Result := False
		end

feature

	async_post_process
		do

		end

end

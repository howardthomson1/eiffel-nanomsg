#include <stdio.h>
#if 1
#include "eif_eiffel.h"
#include "eif_cecil.h"
#include "eif_setup.h"
#endif
#include "nng_include.h"
#include <pthread.h>

/*
 *	-- 0/ Pointer to NNG aio struct
 *	-- 1/ Pointer to socket mutex
 *	-- 2/ Pointer to socket condition_variable
 *	-- 3/ Flag BOOLEAN
 *	-- 4/ Callback routine pointer
 *	-- 5/ Pointer to NNG_AIO instance
 *	--		Must use eif_protect/eif_access
 */

void c_nng_aio_callback(void *a_callback_arg) {
        void **pptr = (void **)a_callback_arg;
        char *flag = (char *) &pptr[3];
        EIF_PROCEDURE e_proc;
        EIF_OBJECT *p_object;
#if 0
        (void) pthread_mutex_lock(pptr[1]);
        if (*flag) {
                pthread_cond_signal(pptr[2]);
        }
        else {
                *flag = 1;
        }
        (void) pthread_mutex_unlock(pptr[1]);
#else
        (void) pthread_mutex_lock(pptr[1]);
        if (*flag) {
                pthread_cond_signal(pptr[2]);
        }
        *flag = 0;
        (void) pthread_mutex_unlock(pptr[1]);
#endif

#if 0
        e_proc = pptr[4];

		printf ("e_proc: %p\n", (void *) e_proc);

        if(e_proc != NULL)
#if 1
            (e_proc)(eif_access(pptr[5]));
#else
            (e_proc)(pptr[5]);
#endif

#endif
}

int c_nng_aio_make(void *item) {
        void **pptr = (void **)item;

        return(nng_aio_alloc (item, &c_nng_aio_callback, item));
}

#if 1
/*
 * See examples/cecil/cecil-test/main.c
 */

int c_nng_aio_set_procedure_target(void *item, EIF_PROCEDURE proc,  EIF_OBJECT target) {
        void **pptr = (void **) (item);
	    EIF_PROCEDURE e_proc;	/* Eiffel procedure `proc_name' */

        e_proc = proc;

        if (e_proc == (EIF_PROCEDURE) 0) {
        	printf ("'apply' procedure not found\n");
            return (0);
        }

        /* Success in finding procedure address, */
        /* now store that and the target into the storage block */

        pptr[4] = e_proc;
        pptr[5] = eif_adopt(target);

        return (1);
}

#endif


void c_call_aio_callback (void *arg) {
        void **pptr = (void **)arg;
        EIF_PROCEDURE e_proc;


   #if 1
        e_proc = pptr[4];

		printf ("e_proc: %p\n", (void *) e_proc);

        if(e_proc != NULL)
#if 1
            (e_proc)(eif_access(pptr[5]));
#else
            (e_proc)(pptr[5]);
#endif

#endif



}

void cecil_set_procedure_target_2(void *a_proc, void *target)
{
	EIF_PROCEDURE e_proc;	/* Eiffel procedure `proc_name' */

	e_proc = (EIF_PROCEDURE)a_proc;
	printf ("Calling back to 'print_callback'\n");
#if 1
	(e_proc)(eif_access(target));
#else
	(e_proc)(target);
#endif
	printf ("cecil_set_procedure_target_2 called ...\n");
}

note
	description: "C API to nng -- NanoMsg Next Generation"
	author: "Howard Thomson"
	date: "$17-Apr-2022$"

class
	NNG_C_API

inherit
	NNG_ERRNO_ENUM_ENUM_API
		rename is_valid_enum as is_valid_nng_errnum end

feature -- C routines in libnng

	nng_version: POINTER
		external
			"C inline use <nng.h>"
		alias
			"return nng_version ();"
		end

feature -- Socket routines

	c_nng_close (a_socket_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>, <nng.h>"
		alias
			"[
				return nng_close (*(nng_socket*)$a_socket_ptr);
			]"
		end

	c_nng_socket_id (a_socket_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_id (*(nng_socket*)$a_socket_ptr);
			]"
		end

feature -- Socket set ...

	c_nng_socket_set (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER; anonymous_4: NATURAL): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (void const*)$anonymous_3, (size_t)$anonymous_4);
			]"
		end

	c_nng_socket_set_bool (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: INTEGER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set_bool (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (_Bool)$anonymous_3);
			]"
		end

	c_nng_socket_set_int (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: INTEGER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set_int (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (int)$anonymous_3);
			]"
		end

	c_nng_socket_set_size (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: NATURAL): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set_size (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (size_t)$anonymous_3);
			]"
		end

	c_nng_socket_set_uint64 (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: NATURAL): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set_uint64 (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (uint64_t)$anonymous_3);
			]"
		end

	c_nng_socket_set_string (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set_string (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (char const*)$anonymous_3);
			]"
		end

	c_nng_socket_set_ptr (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set_ptr (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (void*)$anonymous_3);
			]"
		end

	c_nng_socket_set_ms (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: INTEGER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set_ms (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (nng_duration)$anonymous_3);
			]"
		end

	c_nng_socket_set_addr (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_set_addr (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (nng_sockaddr const*)$anonymous_3);
			]"
		end

feature -- Socket get ...

	c_nng_socket_get (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER; anonymous_4: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (void*)$anonymous_3, (size_t*)$anonymous_4);
			]"
		end

	c_nng_socket_get_bool (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: TYPED_POINTER [INTEGER]): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get_bool (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (_Bool*)$anonymous_3);
			]"
		end

	c_nng_socket_get_int (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: TYPED_POINTER [INTEGER]): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get_int (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (int*)$anonymous_3);
			]"
		end

	c_nng_socket_get_size (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get_size (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (size_t*)$anonymous_3);
			]"
		end

	c_nng_socket_get_uint64 (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get_uint64 (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (uint64_t*)$anonymous_3);
			]"
		end

	c_nng_socket_get_string (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get_string (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (char**)$anonymous_3);
			]"
		end

	c_nng_socket_get_ptr (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get_ptr (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (void**)$anonymous_3);
			]"
		end

	c_nng_socket_get_ms (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get_ms (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (nng_duration*)$anonymous_3);
			]"
		end

	c_nng_socket_get_addr (a_socket_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_socket_get_addr (*(nng_socket*)$a_socket_ptr, (char const*)$anonymous_2, (nng_sockaddr*)$anonymous_3);
			]"
		end


feature


	c_nng_listen (a_socket_ptr: POINTER; a_url_ptr: POINTER; a_listener_ptr: POINTER; a_flags: INTEGER): INTEGER
			-- int nng_listen(nng_socket s, const char *url, nng_listener *lp, int flags);
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_listen (*(nng_socket*)$a_socket_ptr, (char const*)$a_url_ptr, (nng_listener*)$a_listener_ptr, (int)$a_flags);
			]"
		end

	c_nng_dial (a_socket_ptr: POINTER; a_url_ptr: POINTER; a_dialer_ptr: POINTER; a_flags: INTEGER): INTEGER
			-- int nng_dial(nng_socket s, const char *url, nng_dialer *dp, int flags);
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_dial (*(nng_socket*)$a_socket_ptr, (char const*)$a_url_ptr, (nng_dialer*)$a_dialer_ptr, (int)$a_flags);
			]"
		end

	c_nng_send_aio (a_socket_ptr: POINTER; anonymous_2: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_send_aio (*(nng_socket*)$a_socket_ptr, (nng_aio*)$anonymous_2);
			]"
		end

	c_nng_recv_aio (a_socket_ptr: POINTER; anonymous_2: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_recv_aio (*(nng_socket*)$a_socket_ptr, (nng_aio*)$anonymous_2);
			]"
		end


feature -- Asynchronous I/O -- aio

	c_nng_aio_alloc (aio_pptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_alloc ((nng_aio**)$aio_pptr, (void (*) (void *anonymous_1))$anonymous_2, (void*)$anonymous_3);
			]"
		end

	c_nng_aio_free (aio_ptr: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_free ((nng_aio*)$aio_ptr);
			]"
		end


	c_nng_aio_stop (aio_ptr: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_stop ((nng_aio*)$aio_ptr);
			]"
		end

	c_nng_aio_result (aio_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_result ((nng_aio*)$aio_ptr);
			]"
		end

	c_nng_aio_count (aio_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_count ((nng_aio*)$aio_ptr);
			]"
		end

	c_nng_aio_cancel (aio_ptr: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_cancel ((nng_aio*)$aio_ptr);
			]"
		end

	c_nng_aio_abort (aio_ptr: POINTER; anonymous_2: INTEGER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_abort ((nng_aio*)$aio_ptr, (int)$anonymous_2);
			]"
		end

	c_nng_aio_wait (aio_ptr: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_wait ((nng_aio*)$aio_ptr);
			]"
		end

	c_nng_aio_set_msg (aio_ptr: POINTER; a_msg_ptr: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_set_msg ((nng_aio*)$aio_ptr, (nng_msg*)$a_msg_ptr);
			]"
		end

	c_nng_aio_get_msg (aio_ptr: POINTER): POINTER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_get_msg ((nng_aio*)$aio_ptr);
			]"
		end

	c_nng_aio_set_input (aio_ptr: POINTER; anonymous_2: INTEGER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_set_input ((nng_aio*)$aio_ptr, (unsigned)$anonymous_2, (void*)$anonymous_3);
			]"
		end
	c_nng_aio_get_input (aio_ptr: POINTER; anonymous_2: INTEGER): POINTER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_get_input ((nng_aio*)$aio_ptr, (unsigned)$anonymous_2);
			]"
		end

	c_nng_aio_set_output (aio_ptr: POINTER; anonymous_2: INTEGER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_set_output ((nng_aio*)$aio_ptr, (unsigned)$anonymous_2, (void*)$anonymous_3);
			]"
		end

	c_nng_aio_get_output (aio_ptr: POINTER; anonymous_2: INTEGER): POINTER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_get_output ((nng_aio*)$aio_ptr, (unsigned)$anonymous_2);
			]"
		end

	c_nng_aio_set_timeout (aio_ptr: POINTER; anonymous_2: INTEGER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_set_timeout ((nng_aio*)$aio_ptr, (nng_duration)$anonymous_2);
			]"
		end

	c_nng_aio_set_iov (aio_ptr: POINTER; anonymous_2: INTEGER; anonymous_3: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_set_iov ((nng_aio*)$aio_ptr, (unsigned)$anonymous_2, (nng_iov const*)$anonymous_3);
			]"
		end

	c_nng_aio_begin (aio_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_aio_begin ((nng_aio*)$aio_ptr);
			]"
		end

	c_nng_aio_finish (aio_ptr: POINTER; anonymous_2: INTEGER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_finish ((nng_aio*)$aio_ptr, (int)$anonymous_2);
			]"
		end

	c_nng_aio_defer (aio_ptr: POINTER; anonymous_2: POINTER; anonymous_3: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_aio_defer ((nng_aio*)$aio_ptr, (nng_aio_cancelfn)$anonymous_2, (void*)$anonymous_3);
			]"
		end

	c_nng_sleep_aio (anonymous_1: INTEGER; aio_ptr: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_sleep_aio ((nng_duration)$anonymous_1, (nng_aio*)$aio_ptr);
			]"
		end

feature -- Message routines -- nng_msg

	c_nng_msg_alloc (a_message_pptr: POINTER; a_size: INTEGER_64): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_msg_alloc ((nng_msg**)$a_message_pptr, (size_t)$a_size);
			]"
		end

	c_nng_msg_free (a_message_ptr: POINTER)
		external
			"C inline use <nng.h>"
		alias
			"[
				nng_msg_free ((nng_msg*)$a_message_ptr);
			]"
		end

	c_nng_msg_realloc (a_message_ptr: POINTER; anonymous_2: INTEGER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_msg_realloc ((nng_msg*)$a_message_ptr, (size_t)$anonymous_2);
			]"
		end

	c_nng_msg_body (a_message_ptr: POINTER): POINTER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_msg_body ((nng_msg*)$a_message_ptr);
			]"
		end


	c_nng_msg_append (a_message_ptr: POINTER; anonymous_2: POINTER; anonymous_3: INTEGER_64): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_msg_append ((nng_msg*)$a_message_ptr, (void const*)$anonymous_2, (size_t)$anonymous_3);
			]"
		end

	c_nng_msg_chop (a_message_ptr: POINTER; anonymous_2: INTEGER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_msg_chop ((nng_msg*)$a_message_ptr, (size_t)$anonymous_2);
			]"
		end



end

note
	description: "libnng REQ socket opening routines"
	author: "Howard Thomson"
	origin: "This file has been generated by WrapC. Do not edit. Changes will be lost!"
	generator: "Eiffel Wrapper Generator"

class REQ_FUNCTIONS_API

feature -- Access

	nng_req0_open (a_socket: NNG_SOCKET): INTEGER
		do
			Result := c_nng_req0_open (a_socket.item)
		end

	nng_req0_open_raw (a_socket: NNG_SOCKET): INTEGER
		do
			Result := c_nng_req0_open_raw (a_socket.item)
		end

feature {NONE} -- Externals

	c_nng_req0_open (a_socket_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_req0_open ((nng_socket*)$a_socket_ptr);
			]"
		end

	c_nng_req0_open_raw (a_socket_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_req0_open_raw ((nng_socket*)$a_socket_ptr);
			]"
		end

feature -- Externals Address

note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

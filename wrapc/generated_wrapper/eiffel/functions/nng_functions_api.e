note

	description: "This file has been generated by EWG. Do not edit. Changes will be lost!"
	generator: "Eiffel Wrapper Generator"

class NNG_FUNCTIONS_API

inherit NNG_C_API


feature -- Access




	nng_msg_append (anonymous_1: NNG_MESSAGE; anonymous_2: POINTER; anonymous_3: INTEGER_64): INTEGER
		do
			Result := c_nng_msg_append (anonymous_1.item, anonymous_2, anonymous_3)
		end






feature -- EWG Generated, and adapted where necessary to pass tests ...


-- See REQ_FUNCTIONS_API
	--	nng_req0_open       int nng_req0_open(nng_socket *s);
	--	nng_rep0_open       int nng_rep0_open(nng_socket *s);


	nng_listen (a_socket: NNG_SOCKET; a_url: STRING; a_listener: detachable NNG_LISTENER; a_flags: INTEGER): INTEGER
		local
			url_c_string: C_STRING
			listener_ptr: POINTER
		do
			create url_c_string.make (a_url)
			if attached a_listener as al then
				listener_ptr := al.item
			else
				listener_ptr := default_pointer
			end
			Result := c_nng_listen (a_socket.item, url_c_string.item, listener_ptr, a_flags)
		end

	nng_dial (a_socket: NNG_SOCKET; a_url: STRING; a_dialer: detachable NNG_DIALER; a_flags: INTEGER): INTEGER
		local
			url_string: C_STRING
			dialer_ptr: POINTER
		do
			create url_string.make (a_url)
			if attached a_dialer as dl then
				dialer_ptr := dl.item
			else
				dialer_ptr := default_pointer
			end
			Result := c_nng_dial (a_socket.item, url_string.item, dialer_ptr, a_flags)
		end


--	int nng_msg_alloc(nng_msg **msgp, size_t size);

	nng_msg_alloc (a_message: NNG_MESSAGE; a_size: INTEGER_64): INTEGER
		do
			Result := c_nng_msg_alloc (a_message.item, a_size)
		end


--	size_t nng_msg_len(nng_msg *msg);

	nng_msg_len (a_message: NNG_MESSAGE): INTEGER_64
		do
			Result := c_nng_msg_len (a_message.msg_ptr)
		end

	c_nng_msg_len (a_message_ptr: POINTER): INTEGER_64
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_msg_len ((nng_msg const*)$a_message_ptr);
			]"
		end

--	void nng_msg_free(nng_msg *msg);

	nng_msg_free (a_message: NNG_MESSAGE)
		do
			c_nng_msg_free (a_message.msg_ptr)
		end


--	int nng_sendmsg(nng_socket s, nng_msg *msg, int flags);

	nng_sendmsg (a_socket: NNG_SOCKET; a_message: NNG_MESSAGE; a_flags: INTEGER): INTEGER
		do
			Result := c_nng_sendmsg (a_socket.item, a_message.msg_ptr, a_flags)
		end

	c_nng_sendmsg (a_socket_ptr: POINTER; a_message_ptr: POINTER; a_flags: INTEGER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_sendmsg (*(nng_socket*)$a_socket_ptr, (nng_msg*)$a_message_ptr, (int)$a_flags);
			]"
		end

--	int nng_recvmsg(nng_socket s, nng_msg **msgp, int flags);

	nng_recvmsg (a_socket: NNG_SOCKET; a_message: NNG_MESSAGE; a_flags: INTEGER): INTEGER
		do
			Result := c_nng_recvmsg (a_socket.item, a_message.item, a_flags)
		end

	c_nng_recvmsg (a_socket_ptr: POINTER; a_message_pptr: POINTER; a_flags: INTEGER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_recvmsg (*(nng_socket*)$a_socket_ptr, (nng_msg**)$a_message_pptr, (int)$a_flags);
			]"
		end

--	int nng_msg_append_u32(nng_msg *msg, uint32_t val32);


	nng_msg_append_u32 (a_message: NNG_MESSAGE; a_integer_32: INTEGER): INTEGER
		do
			Result := c_nng_msg_append_u32 (a_message.msg_ptr, a_integer_32)
		end

	c_nng_msg_append_u32 (a_message_ptr: POINTER; a_integer_32: INTEGER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_msg_append_u32 ((nng_msg*)$a_message_ptr, (uint32_t)$a_integer_32);
			]"
		end


--	int nng_msg_trim_u32(nng_msg *msg, uint32_t *val32);

	nng_msg_trim_u32 (a_message: NNG_MESSAGE; u32_ptr: POINTER): INTEGER
		do
			Result := c_nng_msg_trim_u32 (a_message.msg_ptr, u32_ptr)
		end

	c_nng_msg_trim_u32 (a_message_ptr: POINTER; u32_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_msg_trim_u32 ((nng_msg*)$a_message_ptr, (uint32_t*)$u32_ptr);
			]"
		end


note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

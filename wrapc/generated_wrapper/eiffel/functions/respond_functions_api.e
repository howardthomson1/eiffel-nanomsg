note
	description: "Summary description for {RESPOND_FUNCTIONS_API}."
	author: "Howard Thomson"
	date: "$6-May-2022$"

class
	RESPOND_FUNCTIONS_API

feature -- Access

	nng_respondent0_open (a_socket: NNG_SOCKET): INTEGER
		do
			Result := c_nng_respondent0_open (a_socket.item)
		end

	nng_respondent0_open_raw (a_socket: NNG_SOCKET): INTEGER
		do
			Result := c_nng_respondent0_open_raw (a_socket.item)
		end

feature {NONE} -- Externals

	c_nng_respondent0_open (a_socket_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_respondent0_open ((nng_socket*)$a_socket_ptr);
			]"
		end

	c_nng_respondent0_open_raw (a_socket_ptr: POINTER): INTEGER
		external
			"C inline use <nng.h>"
		alias
			"[
				return nng_respondent0_open_raw ((nng_socket*)$a_socket_ptr);
			]"
		end

end

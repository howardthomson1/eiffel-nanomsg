note
	description: "Summary description for {NNG_STRUCT_UINT32}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	NNG_STRUCT_UINT32

inherit
	PLATFORM

feature

	make_struct_uint32
		do
			create managed_pointer.make (Integer_32_bytes)
		end

	item: POINTER
		do
			Result := managed_pointer.item
		end

	id: INTEGER_32
		do
			Result := managed_pointer.read_integer_32 (0)
		end

	set_id (an_id: INTEGER_32)
		do
			managed_pointer.put_integer_32 (an_id, 0)
		end

	managed_pointer: MANAGED_POINTER

;note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

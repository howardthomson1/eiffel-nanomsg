note
	description: "Summary description for {NNG_STRUCT_POINTER}."
	author: "Howard Thomson"
	date: "20-May-2020"

deferred class
	NNG_STRUCT_POINTER

inherit
	PLATFORM

feature

	make_pointer
		do
			create managed_pointer.make (Pointer_bytes)
		end

	item: POINTER
		do
			Result := managed_pointer.item
		end

	pointer: POINTER
		do
			Result := managed_pointer.read_pointer (0)
		end

	set_pointer (a_pointer: POINTER)
		do
			managed_pointer.put_pointer (a_pointer, 0)
		end

	managed_pointer: MANAGED_POINTER


end

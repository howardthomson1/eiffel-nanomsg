note

	description: "This file has been generated by EWG. Do not edit. Changes will be lost!"

	generator: "Eiffel Wrapper Generator"

class NNG_SOCKADDR_PATH_STRUCT_API

inherit

	MEMORY_STRUCTURE

	
create

	make,
	make_by_pointer

feature -- Measurement

	structure_size: INTEGER 
		do
			Result := sizeof_external
		end

feature {ANY} -- Member Access

	sa_family: INTEGER
			-- Access member `sa_family`
		require
			exists: exists
		do
			Result := c_sa_family (item)
		ensure
			result_correct: Result = c_sa_family (item)
		end

	set_sa_family (a_value: INTEGER) 
			-- Change the value of member `sa_family` to `a_value`.
		require
			exists: exists
		do
			set_c_sa_family (item, a_value)
		ensure
			sa_family_set: a_value = sa_family
		end

	sa_path: POINTER
			-- Access member `sa_path`
		require
			exists: exists
		do
			Result := c_sa_path (item)
		ensure
			result_correct: Result = c_sa_path (item)
		end

feature {NONE} -- Implementation wrapper for struct struct nng_sockaddr_path

	sizeof_external: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"sizeof(struct nng_sockaddr_path)"
		end

	c_sa_family (an_item: POINTER): INTEGER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <nng.h>"
		alias
			"[
				((struct nng_sockaddr_path*)$an_item)->sa_family
			]"
		end

	set_c_sa_family (an_item: POINTER; a_value: INTEGER) 
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <nng.h>"
		alias
			"[
				((struct nng_sockaddr_path*)$an_item)->sa_family =  (uint16_t)$a_value
			]"
		ensure
			sa_family_set: a_value = c_sa_family (an_item)
		end

	c_sa_path (an_item: POINTER): POINTER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <nng.h>"
		alias
			"[
				((struct nng_sockaddr_path*)$an_item)->sa_path
			]"
		end

end

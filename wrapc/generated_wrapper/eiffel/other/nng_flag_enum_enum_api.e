-- enum wrapper
class NNG_FLAG_ENUM_ENUM_API

feature {ANY}

	is_valid_enum (a_value: INTEGER): BOOLEAN 
			-- Is `a_value' a valid integer code for this enum ?
		do
			Result := a_value = nng_flag_alloc or a_value = nng_flag_nonblock
		end

	nng_flag_alloc: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_FLAG_ALLOC"
		end

	nng_flag_nonblock: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_FLAG_NONBLOCK"
		end

end

-- enum wrapper
class NNG_PIPE_EV_ENUM_API

feature {ANY}

	is_valid_enum (a_value: INTEGER): BOOLEAN 
			-- Is `a_value' a valid integer code for this enum ?
		do
			Result := a_value = nng_pipe_ev_add_pre or a_value = nng_pipe_ev_add_post or a_value = nng_pipe_ev_rem_post or a_value = nng_pipe_ev_num
		end

	nng_pipe_ev_add_pre: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_PIPE_EV_ADD_PRE"
		end

	nng_pipe_ev_add_post: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_PIPE_EV_ADD_POST"
		end

	nng_pipe_ev_rem_post: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_PIPE_EV_REM_POST"
		end

	nng_pipe_ev_num: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_PIPE_EV_NUM"
		end

end

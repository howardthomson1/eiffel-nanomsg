-- enum wrapper
class NNG_SOCKADDR_FAMILY_ENUM_API

feature {ANY}

	is_valid_enum (a_value: INTEGER): BOOLEAN 
			-- Is `a_value' a valid integer code for this enum ?
		do
			Result := a_value = nng_af_unspec or a_value = nng_af_inproc or a_value = nng_af_ipc or a_value = nng_af_inet or a_value = nng_af_inet6 or a_value = nng_af_zt
		end

	nng_af_unspec: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_AF_UNSPEC"
		end

	nng_af_inproc: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_AF_INPROC"
		end

	nng_af_ipc: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_AF_IPC"
		end

	nng_af_inet: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_AF_INET"
		end

	nng_af_inet6: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_AF_INET6"
		end

	nng_af_zt: INTEGER 
		external
			"C inline use <nng.h>"
		alias
			"NNG_AF_ZT"
		end

end

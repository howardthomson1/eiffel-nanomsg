#include <ewg_eiffel.h>
#include <ewg_unknown_callback_c_glue_code.h>

#ifdef _MSC_VER
#pragma warning (disable:4715) // Not all control paths return a value
#endif
struct nng_pipe_cb_entry_struct nng_pipe_cb_entry = {NULL, NULL};

void nng_pipe_cb_stub (nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3)
{
	if (nng_pipe_cb_entry.a_class != NULL && nng_pipe_cb_entry.feature != NULL)
	{
		nng_pipe_cb_entry.feature (eif_access(nng_pipe_cb_entry.a_class), anonymous_1, anonymous_2, anonymous_3);
	}
}

void set_nng_pipe_cb_entry (void* a_class, void* a_feature)
{
	nng_pipe_cb_entry.a_class = eif_adopt(a_class);
	nng_pipe_cb_entry.feature = (nng_pipe_cb_eiffel_feature) a_feature;
}

void* get_nng_pipe_cb_stub ()
{
	return (void*) nng_pipe_cb_stub;
}

void call_nng_pipe_cb (void *a_function, nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3)
{
	((void (*) (nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3))a_function) (anonymous_1, anonymous_2, anonymous_3);
}

struct void_voidp_anonymous_callback_entry_struct void_voidp_anonymous_callback_entry = {NULL, NULL};

void void_voidp_anonymous_callback_stub (void *anonymous_1)
{
	if (void_voidp_anonymous_callback_entry.a_class != NULL && void_voidp_anonymous_callback_entry.feature != NULL)
	{
		void_voidp_anonymous_callback_entry.feature (eif_access(void_voidp_anonymous_callback_entry.a_class), anonymous_1);
	}
}

void set_void_voidp_anonymous_callback_entry (void* a_class, void* a_feature)
{
	void_voidp_anonymous_callback_entry.a_class = eif_adopt(a_class);
	void_voidp_anonymous_callback_entry.feature = (void_voidp_anonymous_callback_eiffel_feature) a_feature;
}

void* get_void_voidp_anonymous_callback_stub ()
{
	return (void*) void_voidp_anonymous_callback_stub;
}

void call_void_voidp_anonymous_callback (void *a_function, void *anonymous_1)
{
	((void (*) (void *anonymous_1))a_function) (anonymous_1);
}

struct nng_aio_cancelfn_entry_struct nng_aio_cancelfn_entry = {NULL, NULL};

void nng_aio_cancelfn_stub (nng_aio *anonymous_1, void *anonymous_2, int anonymous_3)
{
	if (nng_aio_cancelfn_entry.a_class != NULL && nng_aio_cancelfn_entry.feature != NULL)
	{
		nng_aio_cancelfn_entry.feature (eif_access(nng_aio_cancelfn_entry.a_class), anonymous_1, anonymous_2, anonymous_3);
	}
}

void set_nng_aio_cancelfn_entry (void* a_class, void* a_feature)
{
	nng_aio_cancelfn_entry.a_class = eif_adopt(a_class);
	nng_aio_cancelfn_entry.feature = (nng_aio_cancelfn_eiffel_feature) a_feature;
}

void* get_nng_aio_cancelfn_stub ()
{
	return (void*) nng_aio_cancelfn_stub;
}

void call_nng_aio_cancelfn (void *a_function, nng_aio *anonymous_1, void *anonymous_2, int anonymous_3)
{
	((void (*) (nng_aio *anonymous_1, void *anonymous_2, int anonymous_3))a_function) (anonymous_1, anonymous_2, anonymous_3);
}


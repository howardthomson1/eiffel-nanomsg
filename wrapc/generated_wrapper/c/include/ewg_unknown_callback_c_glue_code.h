#ifndef EWG_CALLBACK_UNKNOWN___
#define EWG_CALLBACK_UNKNOWN___

#include <nng_include.h>

typedef void (*nng_pipe_cb_eiffel_feature) (void *a_class, nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3);

void* get_nng_pipe_cb_stub ();

struct nng_pipe_cb_entry_struct
{
	void* a_class;
	nng_pipe_cb_eiffel_feature feature;
};

void set_nng_pipe_cb_entry (void* a_class, void* a_feature);

void call_nng_pipe_cb (void *a_function, nng_pipe anonymous_1, nng_pipe_ev anonymous_2, void *anonymous_3);


#include <nng_include.h>

typedef void (*void_voidp_anonymous_callback_eiffel_feature) (void *a_class, void *anonymous_1);

void* get_void_voidp_anonymous_callback_stub ();

struct void_voidp_anonymous_callback_entry_struct
{
	void* a_class;
	void_voidp_anonymous_callback_eiffel_feature feature;
};

void set_void_voidp_anonymous_callback_entry (void* a_class, void* a_feature);

void call_void_voidp_anonymous_callback (void *a_function, void *anonymous_1);


#include <nng_include.h>

typedef void (*nng_aio_cancelfn_eiffel_feature) (void *a_class, nng_aio *anonymous_1, void *anonymous_2, int anonymous_3);

void* get_nng_aio_cancelfn_stub ();

struct nng_aio_cancelfn_entry_struct
{
	void* a_class;
	nng_aio_cancelfn_eiffel_feature feature;
};

void set_nng_aio_cancelfn_entry (void* a_class, void* a_feature);

void call_nng_aio_cancelfn (void *a_function, nng_aio *anonymous_1, void *anonymous_2, int anonymous_3);


#endif

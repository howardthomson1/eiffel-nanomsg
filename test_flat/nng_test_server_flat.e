note
	description: "Summary description for {NNG_TEST_SERVER}."
	author: "Howard Thomson"
	date: "8-May-2020"

	notes: "[
		Rewite the client and server to remove all the wrapping problems, which the initial wrap_c implementation
		has made getting the code working 'head hurtingly' difficult ...

		Apart from NNG_SOCKET, rewrite all other libnng connections to be local pointers ...
	]"



class
	NNG_TEST_SERVER_FLAT

inherit
	THREAD
		rename
			make as make_thread
		end
	NNG_INTERFACE

create
	make

feature

	net_address: STRING

	socket: NNG_REPLY_SOCKET

	trace_file: PLAIN_TEXT_FILE

	make (a_address: STRING)
			-- Setup for this thread
		do
			make_thread
			net_address := a_address
			create trace_file.make_create_read_write ("/home/ht/Desktop/eac_server_trace"); trace_file.flush
			create socket
		end

	execute
			-- Entry point for this thread
		local
			l_msg: NNG_MESSAGE
			l_value: INTEGER_32
			l_reply: NNG_MESSAGE
			i: INTEGER

			cs: C_STRING
			last_error: INTEGER
			l_recvmsg: POINTER
			l_sendmsg: POINTER
		do
			from
				-- Create the Reply socket
				socket.open
				check socket.last_error = 0 and then socket.is_open end
				trace_file.putstring ("Socket opened%N"); trace_file.flush

					-- socket.listen (net_address)
				create cs.make (net_address)
				last_error := c_nng_listen (socket.item, cs.item, default_pointer, 0)
				trace_file.putstring ("Listener started%N"); trace_file.flush
				i := 1
			until
				i > 10
			loop
				trace_file.putstring ("About to receive%N"); trace_file.flush

					-- Receive message
				last_error := c_nng_recvmsg (socket.item, $l_recvmsg, 0)
				check c_nng_msg_len ($l_recvmsg) = 4 end
				trace_file.putstring ("Message received%N"); trace_file.flush

					-- Extract data ...
				last_error := c_nng_msg_trim_u32 ($l_recvmsg, $l_value)
				check last_error = 0 end
				check l_value = 0xabcd1234 end
				trace_file.putstring ("Data extracted from client message%N"); trace_file.flush

					-- Re-use the incoming message
				l_sendmsg := l_recvmsg; l_recvmsg := default_pointer

					-- Append data to the reply
				last_error := c_nng_msg_append_u32 ($l_sendmsg, 0x5678ffff)
				check last_error = 0 end
				check c_nng_msg_len ($l_sendmsg) = 4 end
				trace_file.putstring ("Data appended to server response%N"); trace_file.flush

					-- socket.send (l_reply)
				last_error := c_nng_sendmsg (socket.item, l_sendmsg, 0)
				check last_error = 0 end
				trace_file.putstring ("Server message sent%N"); trace_file.flush

				trace_file.putstring ("Reply sent%N"); trace_file.flush
				trace_file.putstring ("Message no: " + i.out + "%N"); trace_file.flush
				i := i + 1
			end
			trace_file.putstring ("End of TEST_SERVER thread%N"); trace_file.flush
			trace_file.close


-- What happens as the thread terminates ????
			current.join_all
		end

--	nng_listen (anonymous_1: NNG_SOCKET_S_STRUCT_API; anonymous_2: STRING; anonymous_3: NNG_LISTENER_S_STRUCT_API; anonymous_4: INTEGER): INTEGER
--		local
--			anonymous_2_c_string: C_STRING
--		do
--			create anonymous_2_c_string.make (anonymous_2)
--			Result := c_nng_listen (anonymous_1.item, anonymous_2_c_string.item, anonymous_3.item, anonymous_4)
--		end

--	c_nng_listen (anonymous_1: POINTER; anonymous_2: POINTER; anonymous_3: POINTER; anonymous_4: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_listen (*(nng_socket*)$anonymous_1, (char const*)$anonymous_2, (nng_listener*)$anonymous_3, (int)$anonymous_4);
--			]"
--		end

----	nng_recvmsg (anonymous_1: NNG_SOCKET_S_STRUCT_API; anonymous_2: NNG_MSG_STRUCT_API; anonymous_3: INTEGER): INTEGER
----		do
----			Result := c_nng_recvmsg (anonymous_1.item, anonymous_2.item, anonymous_3)
----		end

--	c_nng_recvmsg (a_socket: POINTER; a_msg_ptr: POINTER; a_flags: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_recvmsg (*(nng_socket*)$a_socket, (nng_msg**)$a_msg_ptr, (int)$a_flags);
--			]"
--		end

--	c_nng_msg_len (a_msg_ptr: POINTER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_msg_len ((nng_msg const*)$a_msg_ptr);
--			]"
--		end

--	c_nng_msg_trim_u32 (a_msg_ptr: POINTER; a_target_ptr: POINTER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_msg_trim_u32 ((nng_msg*)$a_msg_ptr, (uint32_t*)$a_target_ptr);
--			]"
--		end

--	c_nng_msg_append_u32 (a_msg_ptr: POINTER; a_value: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_msg_append_u32 ((nng_msg*)$a_msg_ptr, (uint32_t)$a_value);
--			]"
--		end

--	c_nng_sendmsg (a_socket: POINTER; a_msg_ptr: POINTER; a_flags: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_sendmsg (*(nng_socket*)$a_socket, (nng_msg*)$a_msg_ptr, (int)$a_flags);
--			]"
--		end

end

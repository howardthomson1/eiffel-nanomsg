note
	description: "Summary description for {NNG_CLIENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NNG_TEST_CLIENT_FLAT

inherit
	THREAD
		rename
			make as make_thread
		end
	NNG_INTERFACE

create
	make

feature

	net_address: STRING

	socket: NNG_REQUEST_SOCKET

	trace_file: PLAIN_TEXT_FILE

	make (a_address: STRING)
			-- Setup for this thread
		do
			make_thread
			net_address := a_address
				-- Create Request socket
			create trace_file.make_create_read_write ("/home/ht/Desktop/eac_client_trace"); trace_file.flush
			create socket
		end


	execute
			-- Entry point for this thread
		local
		--	l_msg: NNG_MESSAGE
			i: INTEGER
			cs: C_STRING
			last_error: INTEGER
			l_sendmsg: POINTER
			l_recvmsg: POINTER
			l_value: INTEGER_32
		do
			from
				i := 1
				socket.open
				check socket.last_error = 0 and then socket.is_open end
				trace_file.putstring ("Socket opened%N"); trace_file.flush

					-- Dial the socket
				trace_file.putstring ("Dial starting ...%N"); trace_file.flush
					-- socket.dial (net_address)
				create cs.make (net_address)
				last_error := c_nng_dial (socket.item, cs.item, default_pointer, 0)
				check last_error = 0 end
				trace_file.putstring ("Dialer started%N"); trace_file.flush
			until
				i > 10
			loop
					-- Allocate a message
				last_error := c_nng_msg_alloc ($l_sendmsg, 0)
				check last_error = 0 end
				check c_nng_msg_len ($l_sendmsg) = 0 end

					-- Append some predictable data ...
				last_error := c_nng_msg_append_u32 ($l_sendmsg, 0xabcd1234)
				check last_error = 0 end
				check c_nng_msg_len ($l_sendmsg) = 4 end

					-- Send the request
				last_error := c_nng_sendmsg (socket.item, l_sendmsg, 0)
				check last_error = 0 end

					-- Receive a response
				last_error := current.c_nng_recvmsg (socket.item, $l_recvmsg, 0)
				check last_error = 0 end
				check c_nng_msg_len ($l_recvmsg) = 4 end

					-- Extract the data
				last_error := current.c_nng_msg_trim_u32 ($l_recvmsg, $l_value)
				check c_nng_msg_len ($l_recvmsg) = 0 end

					-- Free the message
				current.c_nng_msg_free ($l_recvmsg)


				trace_file.putstring ("Message created%N"); trace_file.flush

				trace_file.putstring ("Message sent%N"); trace_file.flush

				trace_file.putstring ("Message received%N"); trace_file.flush
		--		print ("Freeing message in Client%N")
				trace_file.putstring ("Freeing message in Client%N"); trace_file.flush
		--		l_msg.msg_free
				trace_file.putstring ("Message no: " + i.out + "%N"); trace_file.flush
				i := i + 1
			end
			trace_file.putstring ("End of TEST_CLIENT thread%N"); trace_file.flush
			trace_file.close

-- What happens as the thread terminates ????
			current.join_all
		end


end

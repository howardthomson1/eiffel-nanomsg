note
	description: "Summary description for {NNG_DIALER}."
	author: "Howard Thomson"

class
	NNG_DIALER

inherit
	NNG_INTERFACE

	NNG_STRUCT_UINT32
		rename
			id as dialer_id
		end

create
	make,
	dial

feature -- Creation

	make (a_socket: NNG_SOCKET; a_url: STRING)
		do
			make_struct_uint32
		end

	dial (a_socket: NNG_SOCKET; a_url: STRING)
		do
			make_struct_uint32
			last_error := nng_dial (a_socket, a_url, Current, 0)
		end

feature -- Attributes and setters

	last_error: INTEGER

;note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

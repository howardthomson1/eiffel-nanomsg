note
	description: "Summary description for {NNG_CONDITION_VARIABLE}."
	author: "Howard Thomson"
	date: "$30/Apr/2022$"

class
	NNG_CONDITION_VARIABLE

inherit
	CONDITION_VARIABLE
		export {NNG_SOCKET, NNG_AIO}
			cond_pointer
		end

create
	make

end

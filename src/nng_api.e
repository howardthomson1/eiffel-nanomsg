note
	description: "API to libnng 'C' interface"
	author: "Howard Thomson"
	date: "24-Dec-2019"
	wrap_c_note: "[
		The parent classes are generated using the wrap_c program:
				
		wrap_c --c_compile_options='-I /path-to-nng-git-repo/nng/include/nng' --full-header=./nng.h
		
		where nng.h consists of:
		
		#include "nng.h"
		#include "protocol/bus0/bus.h"
		#include "protocol/pair0/pair.h"
		#include "protocol/pair1/pair.h"
		#include "protocol/pipeline0/pull.h"
		#include "protocol/pipeline0/push.h"
		#include "protocol/pubsub0/pub.h"
		#include "protocol/pubsub0/sub.h"
		#include "protocol/reqrep0/rep.h"
		#include "protocol/reqrep0/req.h"
	]"

deferred class
	NNG_API

inherit
	NNG_FUNCTIONS_API
		-- Request/Reply protocol REQ_REP
	REQ_FUNCTIONS_API
	REP_FUNCTIONS_API
		-- Bus protocol	
	BUS_FUNCTIONS_API
		-- Pair protocol
	PAIR_FUNCTIONS_API
		-- Publish/Subscribe protocol PUB/SUB
	PUB_FUNCTIONS_API
	SUB_FUNCTIONS_API
		-- Push/Pull protocol
	PUSH_FUNCTIONS_API
	PULL_FUNCTIONS_API
		-- Survey/Respond protocol
	SURVEY_FUNCTIONS_API
	RESPOND_FUNCTIONS_API

feature -- WrapC problems ?

--	c_nng_dial (anonymous_1: POINTER; anonymous_2: POINTER; anonymous_3: POINTER; anonymous_4: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_dial (*(nng_socket*)$anonymous_1, (char const*)$anonymous_2, (nng_dialer*)$anonymous_3, (int)$anonymous_4);
--			]"
--		end

--	c_nng_dial (a_socket: INTEGER_32; a_url: POINTER; a_dialer: POINTER; a_flags: INTEGER): INTEGER
--		external "C inline use <nng/nng.h>"
----		alias "nng_msg_alloc " end
--		alias
--			"[
--				return nng_dial ((int)$a_socket, (char const*)$a_url, (nng_dialer*)$a_dialer, (int)$a_flags);
--			]"
--		end


feature -- Message ops

-- alloc, append, body, chop, clear, dup, free, insert, len, realloc, set_pipe, trim

--	nng_msg_alloc (a_pptr: POINTER; a_size: INTEGER): INTEGER
--			-- Allocate a new nng message
--            -- int nng_msg_alloc(nng_msg **msgp, size_t size);
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_alloc " end

--	nng_msg_append (a_ptr: POINTER; a_buf: POINTER; a_size: INTEGER): INTEGER
--			--	int nng_msg_append(nng_msg *msg, const void *val, size_t size);	
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_append" end

--	nng_msg_append_u16 (a_ptr: POINTER; a_val: INTEGER_16): INTEGER
--			--	int nng_msg_append_u16(nng_msg *msg, uint16_t val16);
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_append_u16" end

--	nng_msg_append_u32 (a_ptr: POINTER; a_val: INTEGER_32): INTEGER
--			--	int nng_msg_append_u32(nng_msg *msg, uint32_t val32);
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_append_u32" end

--	nng_msg_append_u64 (a_ptr: POINTER; a_val: INTEGER_64): INTEGER
--			--	int nng_msg_append_u64(nng_msg *msg, uint64_t val64);
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_append_u64" end

--	nng_msg_body (a_ptr: POINTER): POINTER
--            -- void *nng_msg_body(nng_msg *msg);
--            -- return base address of message body
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_body" end

--	nng_msg_chop (a_ptr: POINTER; a_size: INTEGER): INTEGER
--            -- nng_msg *a_ptr
--            -- remove data from end of message body
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_chop" end

--	nng_msg_chop_u16 (a_ptr: POINTER; a_u16ptr: POINTER): INTEGER
--            -- nng_msg *a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_chop_u16" end

--	nng_msg_chop_u32 (a_ptr: POINTER; a_u32ptr: POINTER): INTEGER
--            -- nng_msg *a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_chop_u32" end

--	nng_msg_chop_u64 (a_ptr: POINTER; a_u64ptr: POINTER): INTEGER
--            -- nng_msg *a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_chop_u64" end

--	nng_msg_clear (a_ptr: POINTER)
--            -- void nng_msg_clear(nng_msg *msg);
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_clear" end

--	nng_msg_dup (a_pptr: POINTER; a_ptr: POINTER): INTEGER
--            -- int nng_msg_dup(nng_msg **dup, nng_msg_t *orig);
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_dup" end

--	--	int nng_msg_insert(nng_msg *msg, const void *val, size_t size);
--	--	int nng_msg_insert(nng_msg *msg, uint16_t val16);
--	--	int nng_msg_insert(nng_msg *msg, uint32_t val32);
--	--	int nng_msg_insert(nng_msg *msg, uint64_t val64);

--	nng_msg_insert (a_ptr: POINTER; a_buf: POINTER; a_size: INTEGER): INTEGER
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_insert" end

--	nng_msg_insert_u16 (a_ptr: POINTER; a_val: INTEGER_16): INTEGER
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_insert_u16" end

--	nng_msg_insert_u32 (a_ptr: POINTER; a_val: INTEGER_32): INTEGER
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_insert_u32" end

--	nng_msg_insert_u64 (a_ptr: POINTER; a_val: INTEGER_64): INTEGER
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_insert_u64" end

--	nng_msg_len (a_ptr: POINTER): INTEGER
--            -- size_t nng_msg_len(nng_msg *msg);
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_len" end




-- realloc, set_pipe, trim

--	nng_msg_free (a_ptr: POINTER)
--            -- void nng_msg_free(nng_msg *msg);
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_free " end

-- append, body, chop, clear, dup, insert, len, realloc, set_pipe, trim


feature -- Message Header ops

-- header, header_append, _chop, _clear, _insert, _len, _trim

--	nng_msg_header (a_ptr: POINTER): POINTER
--            -- nng_msg **a_ptr
--            -- return base address of message header
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header" end

--	nng_msg_header_append (a_ptr: POINTER; a_buf: POINTER; a_size: INTEGER): INTEGER
--            -- nng_msg **a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header_append" end

--	nng_msg_header_append_u16 (a_ptr: POINTER; a_val: INTEGER_16): INTEGER
--            -- nng_msg **a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header_append_u16" end

--	nng_msg_header_append_u32 (a_ptr: POINTER; a_val: INTEGER_32): INTEGER
--            -- nng_msg **a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header_append_u32" end

--	nng_msg_header_append_u64 (a_ptr: POINTER; a_val: INTEGER_64): INTEGER
--            -- nng_msg **a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header_append_u64" end

--	nng_msg_header_chop (a_ptr: POINTER; a_size: INTEGER): INTEGER
--            -- nng_msg **a_ptr
--            -- remove data from end of message header
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header_chop" end

--	nng_msg_header_chop_u16 (a_ptr: POINTER; a_u16ptr: POINTER): INTEGER
--            -- nng_msg **a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header_chop_u16" end

--	nng_msg_header_chop_u32 (a_ptr: POINTER; a_u32ptr: POINTER): INTEGER
--            -- nng_msg **a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header_chop_u32" end

--	nng_msg_header_chop_u64 (a_ptr: POINTER; a_u64ptr: POINTER): INTEGER
--            -- nng_msg **a_ptr
--		external "C inline use <nng/nng.h>"
--		alias "nng_msg_header_chop_u64" end

-- _clear, _insert, _len, _trim

feature -- aio: Asynchronous I/O

-- abort, alloc, begin, cancel, count, defer, finish, free, get_input, get_msg, get_output,
-- result, set_input, set_iov, set_msg, set_output, set_timeout, stop, wait

feature -- Random



note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end


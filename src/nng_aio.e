note
	description: "Summary description for {NNG_AIO}."
	author: "Howard Thomson"
	date: "25-May-2020"

	TODO: "[
		The external C code c_nng_aio_make and other code needs adaptation in order to signal the
		requested termination of the process -- signal the condition_variable on which the socket waits.
		Alternatively, use a timeout on the wait. In either case, testing for the outcome of the send/receive
		is needed to distinguish between successful completion, and timeout / termination signal.

		On callback, provide for the option to call a PROCEDURE ... ?
		
		Re-implement the use of the 'flag', as it's use is currently not consistent:
			Flag False => not busy, or last action completed
			Flag True => action started, callback not yet happened
			Flag False -- reset by 'C' callback routine
	]"

class
	NNG_AIO

inherit
	NNG_STRUCT_POINTER
		redefine
			make_pointer
		end
	NNG_API

create
	make

feature {NONE}

	make_pointer
			-- Redefined to allocate space for:
			-- 1/ Pointer to NNG aio struct
			-- 2/ Pointer to socket mutex
			-- 3/ Pointer to socket condition_variable
			-- 4/ Flag BOOLEAN
			-- 5/ Callback routine pointer
			-- 6/ Pointer to NNG_AIO instance
			--		Must use eif_ routines to pin in place
		do
			create managed_pointer.make (6 * Pointer_bytes)
		end

	make
		local
			l_ptr: POINTER
			l_arg: POINTER
		do
			make_pointer
			last_error := c_nng_aio_make (item)
			check last_error = 0 end
--			last_error := c_nng_aio_set_procedure_target (item, $temp_callback, $Current)
		end

--	temp_callback
--		do
--			print ("temp_callback in NNG_AIO%N")
--		end

	c_nng_aio_make (a_arg: POINTER): INTEGER
			-- Allocate the aio struct and store it's address
		external "C" end

--	c_nng_aio_callback (a_arg: POINTER)
--			-- Signal the condition_variable attached to this aio
--		external "C" end

feature {NNG_SOCKET}

	set_mutex_ptr (a_mutex_ptr: POINTER)
		require
			not_null: a_mutex_ptr /= default_pointer
		do
			managed_pointer.put_pointer (a_mutex_ptr, 1 * Pointer_bytes)
		end

	set_condition_ptr (a_cond_ptr: POINTER)
		require
			not_null: a_cond_ptr /= default_pointer
		do
			managed_pointer.put_pointer (a_cond_ptr, 2 * Pointer_bytes)
		end

	last_error: INTEGER

feature --{NNG_SOCKET, NNG_AIO}

	flag: BOOLEAN
		do
			Result := managed_pointer.read_boolean (24)
		end

feature {NONE}

	set_flag (a_value: BOOLEAN)
		do
			managed_pointer.put_boolean (a_value, 24)
		end

feature -- Sending

	set_message (a_message: NNG_MESSAGE)
		require
			not is_busy
		do
			c_nng_aio_set_msg (pointer, a_message.msg_ptr)
		end

	send (a_socket: NNG_SOCKET)
		require
			not is_busy
		do
			check not flag end
			set_flag (True)
			c_nng_send_aio (a_socket.item, pointer)
		end

feature -- Receiving

	receive (a_socket: NNG_SOCKET)
		require
			not is_busy
		do
				-- void nng_recv_aio(nng_socket s, nng_aio *aio);
			check not flag end
			set_flag (True)
			c_nng_recv_aio (a_socket.item, pointer)
		end

	get_message: NNG_MESSAGE
		require
			not is_busy
			not flag
		do
				-- make from nng_aio_get_msg
			create Result
			Result.set_msg_ptr (c_nng_aio_get_msg (pointer))

			check Result.msg_ptr /= default_pointer end
			check Result.has_content end
			check Result.count /= 0 end
		end

feature -- Status checking

	aio_result: INTEGER
		do
			Result := c_nng_aio_result (pointer)	-- ???
		end

--	signal
--			-- Signal the condition_variable
--		do
--			check False end
--			c_nng_aio_callback (item)
--		end

feature -- Completion PROCEDURE callback

	set_callback (an_agent: PROCEDURE)
		local
			res: INTEGER
		do
			callback := an_agent
	--		res := c_nng_aio_set_procedure_target (item, $call_callback, $Current)
	--		check res /= 0 end

--?			c_call_aio_callback (item)
		end

	callback: detachable PROCEDURE

	call_callback
		do
			if attached callback as c then
				c.call ([])

	--			print ("XXXX%N")
			end
		end

	is_busy: BOOLEAN
		do
--			Result := c_nng_aio_busy (pointer) /= 0
			Result := False
		end

	await_completion
		do
			c_nng_aio_wait (pointer)
		end

feature {NONE}

	c_nng_aio_set_procedure_target (p: POINTER; proc: POINTER; call_target: ANY): INTEGER
		external "C" end

	c_call_aio_callback (p: POINTER)
		external "C" end

	c_nng_aio_busy (p: POINTER): INTEGER
		external "C use %"nng.h%""
		alias "nng_aio_busy" end

end

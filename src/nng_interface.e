note
	description: "Summary description for {NNG_INTERFACE}."
	author: "Howard Thomson"
	web_page_links: "[
		libnng(3) Manual Page: https://nng.nanomsg.org/man/v1.3.0/libnng.3.html

		Home page: https://nng.nanomsg.org/
	]"

class
	NNG_INTERFACE

inherit
	NNG_API
		redefine
--			c_nng_listen,
--			c_nng_recvmsg,
--		--	c_nng_msg_len,
--			c_nng_msg_trim_u32,
--			c_nng_msg_append_u32,
--			c_nng_sendmsg,
--			c_nng_dial,
--			c_nng_msg_alloc,
--			c_nng_msg_free
		end
create
	default_create

feature -- Redefinitions -- these work, some are modified from wrap_c generated versions

--	c_nng_listen (anonymous_1: POINTER; anonymous_2: POINTER; anonymous_3: POINTER; anonymous_4: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_listen (*(nng_socket*)$anonymous_1, (char const*)$anonymous_2, (nng_listener*)$anonymous_3, (int)$anonymous_4);
--			]"
--		end

--	c_nng_recvmsg (a_socket: POINTER; a_msg_ptr: POINTER; a_flags: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_recvmsg (*(nng_socket*)$a_socket, (nng_msg**)$a_msg_ptr, (int)$a_flags);
--			]"
--		end

--	XX_c_nng_msg_len (a_msg_ptr: POINTER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_msg_len ((nng_msg const*)$a_msg_ptr);
--			]"
--		end

--	c_nng_msg_trim_u32 (a_msg_pptr: POINTER; a_target_ptr: POINTER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_msg_trim_u32 (*(nng_msg**)$a_msg_pptr, (uint32_t*)$a_target_ptr);
--			]"
--		end

--	c_nng_msg_append_u32 (a_msg_pptr: POINTER; a_value: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_msg_append_u32 (*(nng_msg**)$a_msg_pptr, (uint32_t)$a_value);
--			]"
--		end

--	c_nng_sendmsg (a_socket: POINTER; a_msg_ptr: POINTER; a_flags: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_sendmsg (*(nng_socket*)$a_socket, (nng_msg*)$a_msg_ptr, (int)$a_flags);
--			]"
--		end

--	c_nng_msg_alloc (a_msg_pptr: POINTER; a_size: INTEGER): INTEGER
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				return nng_msg_alloc ((nng_msg**)$a_msg_pptr, (size_t)$a_size);
--			]"
--		end

--	c_nng_msg_free (anonymous_1: POINTER)
--		external
--			"C inline use <nng.h>"
--		alias
--			"[
--				nng_msg_free (*(nng_msg**)$anonymous_1);
--			]"
--		end


feature -- Message routines

feature -- Version information

	version: STRING
		do
			create Result.make_from_c (nng_version)
		end

note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

note
	description: "Summary description for {NNG_MEMORY_READER_WRITER}."
	author: "Howard Thomson"
	date: "16-Feb-2022"
	revision: "$Revision$"

	extended_description: "[
		Descendant of NNG_MEMORY_READER_WRITER to provide access to
		buffer space address, without needlessly copying an additional
		time into a new MANAGED_POINTER [see SED_MEMORY_READER_WRITER.data]
	]"

class
	NNG_MEMORY_READER_WRITER

inherit
	SED_MEMORY_READER_WRITER
		export {NNG_SOCKET} all end

create
	make

feature {NONE}

end

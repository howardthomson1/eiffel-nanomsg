note
	description: "Summary description for {NNG_CLIENT}."
	author: "Howard Thomson"
	date: "$14-Apr-2022$"
	revision: "$Revision$"

	TODO: "[
		Asynchronous receive, as needed for EiffelVision ...
		
		Multiple client test threads, with asynchronous server receive.
	]"

class
	NNG_TEST_CLIENT

inherit
	THREAD
		rename
			make as make_thread
		end

create
	make

feature

	net_address: STRING

	socket: NNG_REQUEST_SOCKET

	make (a_address: STRING)
			-- Setup for this thread
		do
			make_thread
			net_address := a_address.twin
				-- Create Request socket
			create socket
		end

	execute
		do
			print ("Client thread starting ...%N")
			socket.open
			check socket.last_error = 0 and then socket.is_open end

				-- Dial the socket

				-- socket.dial (net_address)
			socket.dial (net_address)
			check socket.last_error = 0 end

			test_1
--			print ("test 2 skipped ...%N")
			test_2
			socket.close
			check socket.last_error = 0 end
		end

	test_1
			-- Entry point for this thread
		local
			l_recvmsg: NNG_MESSAGE
			l_sendmsg: NNG_MESSAGE
			i: INTEGER
			l_value: INTEGER_32
		do
			print ("Starting test #1 [Synchronous send/receive] ...%N")
			from
				i := 1
			until
				i > 1_000
			loop

					-- Allocate a message
				create l_sendmsg.make_size (0);
				check size_on_creation_is_0: l_sendmsg.nng_msg_len (l_sendmsg) = 0 end

					-- Append some predictable data ...
				l_sendmsg.write_integer_32 (0xabcd1234)

				check l_sendmsg.last_error = 0 end
				check size_after_append_is_4: l_sendmsg.nng_msg_len (l_sendmsg) = 4 end

					-- Send the request
				check socket.sending end
				socket.send (l_sendmsg)
				check socket.last_error = 0 end
				check not socket.is_busy end

					-- Receive a response
				check socket.receiving end
				socket.receive
				l_recvmsg := socket.last_message
				check received_message_size_is_4: l_recvmsg.nng_msg_len (l_recvmsg) = 4 end

					-- Extract the data
				l_value := l_recvmsg.trim_integer_32
				check l_recvmsg.last_error = 0 end
				check size_after_trim_is_0: l_recvmsg.nng_msg_len (l_recvmsg) = 0 end
				check l_value = 0x5678ffff or else l_value = 0x12348765 end

					-- Free the message
				l_recvmsg.msg_free

				if (i \\ 1000) = 0 then
					print ("Client msg no: " + i.out + "%N")
				end
				i := i + 1
			end
		end

	test_2_loop_count: INTEGER = 10_000

	test_2_terminate_on_exit: BOOLEAN = False

	test_2
			-- As test_1, but using asynchronous receive, and then wait_for arrival
		local
			l_recvmsg: NNG_MESSAGE
			l_sendmsg: NNG_MESSAGE
			i: INTEGER
			l_value: INTEGER_32
		do
			print ("Starting test #2 [Asynchronous receipt] ...%N")
			apply_check
--			socket.set_aio_procedure_target (agent report_completion)
			from
				i := 1
			until
				i > test_2_loop_count
			loop

					-- Allocate a message
				create l_sendmsg.make_size (0);
				check size_on_creation_is_0: l_sendmsg.nng_msg_len (l_sendmsg) = 0 end

					-- Append some predictable data ...
				if i >= test_2_loop_count and test_2_terminate_on_exit then
					l_sendmsg.write_integer_32 (0xabcdffff) -- Send 'terminate' message
				else
					l_sendmsg.write_integer_32 (0xabcd1234)
				end
				check l_sendmsg.last_error = 0 end
				check size_after_append_is_4: l_sendmsg.nng_msg_len (l_sendmsg) = 4 end

				print ("Sending client msg no: " + i.out + "%N")


					-- Send the request
				socket.send_async (l_sendmsg)
				check socket.last_error = 0 end

					-- Await confirmation of message despatched
				socket.await_aio_completion
				socket.async_post_process

					-- Receive a response
				socket.receive_async (agent do_nothing)

				socket.await_aio_completion

				l_recvmsg := socket.last_message
				check received_message_size_is_4: l_recvmsg.nng_msg_len (l_recvmsg) = 4 end
				socket.async_post_process
				
					-- Extract the data
				l_value := l_recvmsg.trim_integer_32
				check l_recvmsg.last_error = 0 end
				check size_after_trim_is_0: l_recvmsg.nng_msg_len (l_recvmsg) = 0 end
				check l_value = 0x5678ffff or else l_value = 0x12348765 end

					-- Free the message
				l_recvmsg.msg_free

--				if (i \\ 1000) = 0 then
					print ("Client msg no: " + i.out + "%N")
--				end
				i := i + 1
			end

		end

feature -- callback

	report_completion
		do
			print ("report_completion ... OK%N")
		end

	apply_check
		local
			p: PROCEDURE
		do
			p := agent report_completion
			p.apply

			cecil_set_procedure_target_2 ($report_completion, Current)
		end

	cecil_set_procedure_target_2 (a_proc_address: POINTER; a_target: ANY)
		external "C" end

feature -- Testing ... Reference only

	nng_pull_socket: detachable NNG_PULL_SOCKET
	nng_push_socket: detachable  NNG_PUSH_SOCKET

	nng_pair_socket: detachable NNG_PAIR_SOCKET

	nng_survey_socket: detachable NNG_SURVEY_SOCKET
	nng_respond_socket: detachable NNG_RESPOND_SOCKET

	nng_pub_socket: detachable NNG_PUB_SOCKET
	nng_sub_socket: detachable NNG_SUB_SOCKET

;note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

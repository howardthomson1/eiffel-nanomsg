note
	description: "Summary description for {NNG_MESSAGE}."
	author: "Howard Thomson"

	TODO: "[
		Resolve how 'ownership' of the allocated memoery for an NNG_MESSAGE is sustained.
		1/ All C memory allocation is done using the Eiffel runtime, implying  message
			copy for send/receive with libnng
			
		2/ All C memory allocation is done using libnng routines ...
		
		3/ Class NNG_MESSAGE provides for, and correctly retains, the status of which
			runtime side [Eiffel/nng] allocated the memory

		Fold external routines back into NNG_INTERFACE, and it's ancestors
	]"

class
	NNG_MESSAGE

inherit
	NNG_INTERFACE
		export {NONE} all
		redefine default_create
		end

	PLATFORM
		redefine default_create end

	DISPOSABLE
		redefine default_create end

create
	default_create,
	make_size,
	make_from_memory_reader_writer

feature {none} -- Creation

	default_create
		do
			create managed_pointer.make (Pointer_bytes)
			check null_msg_ptr: msg_ptr = default_pointer end
		end

	make_size (a_size: INTEGER)
		require
			a_size >= 0
		do
			default_create
			last_error := nng_msg_alloc (Current, a_size)
			check last_error = 0 end
			check nng_msg_len (Current) = a_size end
		ensure
			valid_msg_ptr: msg_ptr /= default_pointer
		end

	make_from_memory_reader_writer (a_reader_writer: SED_MEMORY_READER_WRITER)
		require
			has_content: a_reader_writer.count > 0
		do
			default_create
--print ("make_from_memory_reader_writer: count = " + a_reader_writer.count.out + "%N")
			last_error := nng_msg_alloc (Current, a_reader_writer.count)
			check allocated_ok: last_error = 0 end
				-- Copy the data ...
			msg_body_ptr.memory_copy (a_reader_writer.buffer.item, a_reader_writer.count)
		end

feature

	has_content: BOOLEAN
		do
			Result := msg_ptr /= default_pointer
			and then nng_msg_len (Current) > 0
		end

	msg_ptr: POINTER
			-- Pointer to libnng's message struct
		do
			Result := managed_pointer.read_pointer (0)
		end

	set_msg_ptr (a_message_ptr: POINTER)
		do
			managed_pointer.put_pointer (a_message_ptr, 0)
		end

	item: POINTER
			-- Address of the pointer to NNG's struct
		do
			Result := managed_pointer.item
		end

	msg_body_ptr: POINTER
			-- Pointer to the body of the message
		do
			Result := c_nng_msg_body (msg_ptr)
		end

	managed_pointer: MANAGED_POINTER
		-- Storage for the link to NNG's message pointer

-----------------------------------------------------


	msg_free
			-- Free the message when done ...
		do
			dispose
		end

	last_error: INTEGER

	append_integer_32, write_integer_32 (i: INTEGER_32)
		local
			old_size: INTEGER_64
		do
			old_size := nng_msg_len (Current)
			last_error := nng_msg_append_u32 (current, i)
			check nng_msg_len (Current) = (old_size + 4) end
		end

	trim_integer_32: INTEGER_32
		local
			r32: INTEGER_32
			old_size: INTEGER_64
		do
			old_size := nng_msg_len (Current)
			last_error := nng_msg_trim_u32 (Current, $r32)
			Result := r32
			check nng_msg_len (Current) = (old_size - 4) end
		end

	count: INTEGER
		require
			msg_ptr /= default_pointer
		do
			Result := nng_msg_len (Current).as_integer_32
		end

feature {NONE}

	dispose
		do
			if msg_ptr /= default_pointer then
				c_nng_msg_free (msg_ptr)
				set_msg_ptr (default_pointer)
			end
		end

invariant
--	position_positive: position >= 0
--	position_within_buffer: position <= count
end

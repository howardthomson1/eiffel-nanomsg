note
	description: "Summary description for {NNG_MUTEX}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	NNG_MUTEX

inherit
	MUTEX
		export {NNG_SOCKET, NNG_AIO}
			mutex_pointer
		end
create
	make
end

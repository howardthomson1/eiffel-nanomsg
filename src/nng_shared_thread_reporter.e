note
	description: "Summary description for {NNG_SHARED_THREAD_REPORTER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NNG_SHARED_THREAD_REPORTER

feature

	thread_reporter (a_string: STRING): NNG_THREAD_REPORTER
		once ("thread")
			create Result.make (a_string)
		end
note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

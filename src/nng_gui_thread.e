note
	description: "Summary description for {NNG_GUI}."
	author: "Howard Thomson"
	date: "1-March-2022"
	revision: "$Revision$"

class
	NNG_GUI_THREAD

inherit
	THREAD

create
	make

feature {NONE} -- Attributes


feature {NONE} -- Initialization

	execute
			-- Initialize and launch application
		local
			gui: NNG_GUI
		do
			create gui.make
		end

end

note
	description: "Summary description for {NNG_TEST_SERVER}."
	author: "Howard Thomson"
	date: "5-May-2020"

	TODO: "[
		Recognise a QUIT message, instaed of using a counted loop.
		
		Add a timeout to the receive call. Use nng_setopt_ms()
	]"

class
	NNG_TEST_SERVER

inherit
	THREAD
		rename
			make as make_thread
		end
	MEMORY

create
	make

feature

	net_address: STRING

	socket: NNG_REPLY_SOCKET

	make (a_address: STRING)
			-- Setup for this thread
		do
			make_thread
			net_address := a_address.twin
			create socket
		end

	execute
			-- Entry point for this thread
		local
			l_msg: NNG_MESSAGE
			l_value: INTEGER_32
			l_reply: NNG_MESSAGE
			i: INTEGER
			quit_received: BOOLEAN
		do
			print ("Server thread starting ...%N")
				-- Create the Reply socket
			socket.open
			check socket.last_error = 0 and then socket.is_open end
			socket.listen (net_address)
			socket.set_receive_timeout (2_000) -- 2 seconds
			from
				i := 1
				quit_received := False
			until
				quit_received
			loop

					-- Receive request
				socket.receive
				check not socket.is_busy end
				if socket.last_error = 0 then
						-- Message received OK
					l_msg := socket.last_message

					check server_rcvd_msg_len_is_4: l_msg.nng_msg_len (l_msg) = 4 end
					l_value := l_msg.trim_integer_32
					check l_msg.count = 0 end
					l_msg.msg_free

					if l_value = 0xabcdffff then
						quit_received := True
					end

						-- Send reply
					create l_reply.make_size (0)
					check l_reply.count = 0 end
					if l_value = 0xabcd1234 then
						l_reply.append_integer_32 (0x5678ffff)
					else
						l_reply.append_integer_32 (0x12348765)
					end
					check sent_message_size_is_4: l_reply.count = 4 end
					socket.send (l_reply)
					check not socket.is_busy end
					check l_reply.msg_ptr = default_pointer end

						print ("Server msg no: " + i.out + "%N")
				else
					quit_received := True
				end
				i := i + 1
			end
			socket.close
		end

note
	copyright: "Copyright (c) 1984-2020, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end

note
	description: "Summary description for {NNG_LISTENER}."
	author: "Howard Thomson"
	date: "10-May-2020"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"

class
	NNG_LISTENER

inherit
	NNG_INTERFACE

	NNG_STRUCT_UINT32
		rename
			id as listener_id
		end

create
	make,
	listen

feature -- Creation

	make (a_socket: NNG_SOCKET; a_url: STRING)
		do
			make_struct_uint32
		end

	listen (a_socket: NNG_SOCKET; a_url: STRING)
		do
			make_struct_uint32
			last_error := nng_listen (a_socket, a_url, Current, 0)
		end

feature -- Attributes and setters

	last_error: INTEGER

end

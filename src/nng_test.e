﻿note
    description: "Starting point for testing NNG"

class
	NNG_TEST

create
	make

feature -- Attributes

	test_client: detachable NNG_TEST_CLIENT
	test_server: detachable NNG_TEST_SERVER
	gui: detachable NNG_GUI_THREAD

feature {NONE} -- Initialization

	make
		local
			l_client: NNG_TEST_CLIENT
			l_server: NNG_TEST_SERVER
	--		l_gui: NNG_GUI_THREAD
			l_address: STRING
		do
			l_address := "inproc://rot13"
				-- Start server, before client so that the listen call happens before
				-- the dial call, essential for [current] libnng implementation ...
			create l_server.make (l_address); test_server := l_server; l_server.launch
				-- Start client, after server
			create l_client.make (l_address); test_client := l_client; l_client.launch
		end

note
	license:	"Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
end
